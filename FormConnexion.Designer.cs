﻿namespace SuiviA
{
    partial class FormConnexion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConnexion));
            this.ConnexionTabPage = new System.Windows.Forms.TabPage();
            this.Connexionlabel = new System.Windows.Forms.Label();
            this.ConnexionBTN = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.co_password = new System.Windows.Forms.TextBox();
            this.UsernameLabel = new System.Windows.Forms.Label();
            this.PswdLabel = new System.Windows.Forms.Label();
            this.co_username = new System.Windows.Forms.TextBox();
            this.MenuConnexionTab = new System.Windows.Forms.TabControl();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.ConnexionTabPage.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.MenuConnexionTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // ConnexionTabPage
            // 
            this.ConnexionTabPage.Controls.Add(this.checkBox1);
            this.ConnexionTabPage.Controls.Add(this.Connexionlabel);
            this.ConnexionTabPage.Controls.Add(this.ConnexionBTN);
            this.ConnexionTabPage.Controls.Add(this.groupBox1);
            this.ConnexionTabPage.Location = new System.Drawing.Point(4, 22);
            this.ConnexionTabPage.Name = "ConnexionTabPage";
            this.ConnexionTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.ConnexionTabPage.Size = new System.Drawing.Size(296, 241);
            this.ConnexionTabPage.TabIndex = 0;
            this.ConnexionTabPage.Text = "Connexion";
            this.ConnexionTabPage.UseVisualStyleBackColor = true;
            // 
            // Connexionlabel
            // 
            this.Connexionlabel.AutoSize = true;
            this.Connexionlabel.Location = new System.Drawing.Point(102, 26);
            this.Connexionlabel.Name = "Connexionlabel";
            this.Connexionlabel.Size = new System.Drawing.Size(84, 13);
            this.Connexionlabel.TabIndex = 1;
            this.Connexionlabel.Text = "Connectez vous";
            // 
            // ConnexionBTN
            // 
            this.ConnexionBTN.Location = new System.Drawing.Point(29, 197);
            this.ConnexionBTN.Name = "ConnexionBTN";
            this.ConnexionBTN.Size = new System.Drawing.Size(239, 23);
            this.ConnexionBTN.TabIndex = 4;
            this.ConnexionBTN.Text = "Connexion";
            this.ConnexionBTN.UseVisualStyleBackColor = true;
            this.ConnexionBTN.Click += new System.EventHandler(this.ConnexionBTN_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.co_password);
            this.groupBox1.Controls.Add(this.UsernameLabel);
            this.groupBox1.Controls.Add(this.PswdLabel);
            this.groupBox1.Controls.Add(this.co_username);
            this.groupBox1.Location = new System.Drawing.Point(46, 54);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 100);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            // 
            // co_password
            // 
            this.co_password.Location = new System.Drawing.Point(84, 57);
            this.co_password.Name = "co_password";
            this.co_password.Size = new System.Drawing.Size(100, 20);
            this.co_password.TabIndex = 3;
            // 
            // UsernameLabel
            // 
            this.UsernameLabel.AutoSize = true;
            this.UsernameLabel.Location = new System.Drawing.Point(11, 31);
            this.UsernameLabel.Name = "UsernameLabel";
            this.UsernameLabel.Size = new System.Drawing.Size(61, 13);
            this.UsernameLabel.TabIndex = 0;
            this.UsernameLabel.Text = "Username :";
            // 
            // PswdLabel
            // 
            this.PswdLabel.AutoSize = true;
            this.PswdLabel.Location = new System.Drawing.Point(11, 60);
            this.PswdLabel.Name = "PswdLabel";
            this.PswdLabel.Size = new System.Drawing.Size(59, 13);
            this.PswdLabel.TabIndex = 1;
            this.PswdLabel.Text = "Password :";
            // 
            // co_username
            // 
            this.co_username.Location = new System.Drawing.Point(84, 28);
            this.co_username.Name = "co_username";
            this.co_username.Size = new System.Drawing.Size(100, 20);
            this.co_username.TabIndex = 2;
            // 
            // MenuConnexionTab
            // 
            this.MenuConnexionTab.Controls.Add(this.ConnexionTabPage);
            this.MenuConnexionTab.Location = new System.Drawing.Point(13, 13);
            this.MenuConnexionTab.Name = "MenuConnexionTab";
            this.MenuConnexionTab.SelectedIndex = 0;
            this.MenuConnexionTab.Size = new System.Drawing.Size(304, 267);
            this.MenuConnexionTab.TabIndex = 0;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(46, 160);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(101, 17);
            this.checkBox1.TabIndex = 7;
            this.checkBox1.Text = "ActiveDirectory ";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // FormConnexion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(329, 291);
            this.Controls.Add(this.MenuConnexionTab);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormConnexion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Authentification";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ConnexionTabPage.ResumeLayout(false);
            this.ConnexionTabPage.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.MenuConnexionTab.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage ConnexionTabPage;
        private System.Windows.Forms.Button ConnexionBTN;
        private System.Windows.Forms.TextBox co_password;
        private System.Windows.Forms.TextBox co_username;
        private System.Windows.Forms.Label PswdLabel;
        private System.Windows.Forms.Label UsernameLabel;
        private System.Windows.Forms.TabControl MenuConnexionTab;
        private System.Windows.Forms.Label Connexionlabel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}