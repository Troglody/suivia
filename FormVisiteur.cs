﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using static System.Net.WebRequestMethods;
using System.Text.RegularExpressions;
using EasyHttp.Http;
   

namespace SuiviA
{
    public partial class FormVisiteur : Form
    {
  
        public FormVisiteur()
        {
            InitializeComponent();
            
        }


        private void FormVisiteur_Load(object sender, EventArgs e)
        {
             LoadComboBox(); //Charge les médecins dans le select
 
             this.BindGrid(); //Charge les visites dans le DataGridView

            DateVisitePicker.Format = DateTimePickerFormat.Custom; //Les lignes suivantes applique un format au composant DataTimePicker
            DateVisitePicker.CustomFormat = "yyyy-MM-dd"; //Format Date en année-mois-jour

            HeureArrivePicker.Format = DateTimePickerFormat.Time; //Format Temps
            HeureArrivePicker.CustomFormat = "HH:mm tt";
            HeureArrivePicker.ShowUpDown = true;

            HeureDepartPicker.Format = DateTimePickerFormat.Time;
            HeureDepartPicker.CustomFormat = "HH:mm tt";
            HeureDepartPicker.ShowUpDown = true;

            StatsChoixJour_Picker.Format = DateTimePickerFormat.Custom; //Les lignes suivantes applique un format au composant DataTimePicker
            StatsChoixJour_Picker.CustomFormat = "yyyy-MM-dd"; //Format Date en année-mois-jour

            StatMain_Label.Text += User.prenom + " " + User.nom;
             DisplayStats(); //Charge les calculs statistiques
        }


        private void disconnectBTN_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormConnexion f1 = new FormConnexion();
            f1.ShowDialog();
        }


        private void BindGrid() // Affiche les visites dans le datagridview
        {
            Cursor.Current = Cursors.WaitCursor;          
            DatagridviewVisite.DataSource = Service.getVisiteID(User.id);
            Cursor.Current = Cursors.Default;
            // DatagridviewVisite.Columns[2].DefaultCellStyle.Format = "dd/MM/yyyy";
        }
 
    
        private void LoadComboBox() 
        {
            QuelMedecinComboBox.Items.Clear();

            List<Medecin> medecins = Service.getMedecin();

            foreach(var med in medecins)
            {
                QuelMedecinComboBox.Items.Add(med.idMedecin + " - " + med.nom + " " + med.prenom);
                QuelMedecinComboBox.SelectedIndex = 0;
            }

        } //Fonction qui charge les medecins

        
        private void AjoutVisiteBtn_Click(object sender, EventArgs e) 
        {
            if (RdvCheckbox.Text != "" && DateVisitePicker.Text != "" && HeureArrivePicker.Text != "" && HeureDepartPicker.Text != "" && QuelMedecinComboBox.SelectedItem.ToString().Split('-')[0] != "")
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    
                    Visite visite = new Visite(DateVisitePicker.Text, RdvCheckbox.Text, HeureArrivePicker.Text, HeureDepartPicker.Text, Convert.ToInt32(QuelMedecinComboBox.Text.Trim().Split('-')[0]));

                    if (Service.post("api/visite", visite))//Ajoute une visite, vérifie les erreurs
                    {
                        MessageBox.Show("Visite ajoutée !");
                        BindGrid();
                    }
                    else { MessageBox.Show("Erreur lors de l'ajout, veuillez ressayer !");  }                      
                }
                catch (Exception a) { MessageBox.Show(a.Message); }
                Cursor.Current = Cursors.Default;
            }
            else { MessageBox.Show("Avez vous oublié un champs?"); }
        }//Ajout en BDD d'une visite

     
        private void RdvCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (RdvCheckbox.Checked)
                this.RdvCheckbox.Text = "oui";
            else
                this.RdvCheckbox.Text = "non";
        }   //Change le texte "Oui/non" sur la checkbox d'ajout de visite

       
        private void dataGridViewVisites_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            Cursor.Current = Cursors.WaitCursor;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && //Limite le (click sur n'importe quelle cellule), au bouton
             e.RowIndex >= 0)
            {
                var identifiant = DatagridviewVisite.Rows[e.RowIndex].Cells["idVisite"].Value;

                DialogResult dialogResult = MessageBox.Show("Êtes vous sur de vouloir supprimer cette visite ?", "Attention", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    if (Service.supprimervisite("api/visite/" + Convert.ToInt32(identifiant))) //Supression, vérification erreur
                    {
                        MessageBox.Show("Visite supprimé avec succès !");
                        BindGrid();
                    }
                    else { MessageBox.Show("Échec de la suppression"); }
                }
            }
            Cursor.Current = Cursors.Default;
        } //Supprimer une visite ,  click sur le boutton du DatagridView


        private void DisplayStats()//temps total
        {
            string SelectedDay = StatsChoixJour_Picker.Text;
           
            try
            {
                List<Timetotal> timetots;

                timetots = Service.GetTotalWait(User.id);
                timetots.ForEach(t =>
                {
                    TempsAttTotal_Content.Text = t.sumtime.ToString();
                }); //Label temps total 
            }
            catch {
                //MessageBox.Show("Une erreur est survenue \n  -  Vérifiez la connexion au serveur local ou à la base de données", "Attention!"); 
            }

            List<TimetotalJour> TTJ;
            List<TimetotalJour> VisiteJour;

            try
            {
                TTJ = Service.GetTimeTotalJour(User.id, SelectedDay);
                TTJ.ForEach(t =>
                {
                    TempsAttenteJour_Content.Text = "";
                    TempsAttenteJour_Content.Text = t.TempsTotalJour;
                });

                VisiteJour = Service.getNbrVisiteparJour(User.id, StatsChoixJour_Picker.Text);
                VisiteJour.ForEach(t =>
                {
                    VisiteJour_Content.Text = t.TempsTotalJour;
                });
            }
            catch
            {
                //MessageBox.Show("Une erreur est survenue \n  -  Vérifiez la connexion au serveur local ou à la base de données", "Attention!"); 
            }
        }


         


        private void TempsAttenteTotal_BTN_Click(object sender, EventArgs e)
        {
 
            FormStatistiques f_TAT = new FormStatistiques();

            f_TAT.stat.DataSource = Service.GetTimeDiff(User.id);
            SuiviA.Statistiques.WhatStat = "Temps d'attente total";
            f_TAT.Show();
 
        }

 
        private void StatsChoixJour_Picker_ValueChanged(object sender, EventArgs e)
        {
            DisplayStats();
        }

        private void TempsChaqueVis_BTN_Click(object sender, EventArgs e)
        {
            try
            {
                FormStatistiques f_TAT = new FormStatistiques();

                f_TAT.stat.DataSource = Service.GetTimeDiff(User.id);
                SuiviA.Statistiques.WhatStat = "Temps d'attente total";
                f_TAT.Show();
            }
            catch
            {
                MessageBox.Show("Une erreur est survenue \n  -  Vérifiez la connexion au serveur local ou à la base de données", "Attention!");
            }
        }
    }
}