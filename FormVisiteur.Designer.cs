﻿using System.Drawing;

namespace SuiviA
{
    partial class FormVisiteur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormVisiteur));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.disconnectBTN = new System.Windows.Forms.Button();
            this.MenuVisite = new System.Windows.Forms.TabControl();
            this.AjouterVisite = new System.Windows.Forms.TabPage();
            this.AddVisiteHeaderLabel = new System.Windows.Forms.Label();
            this.HeureDepartPicker = new System.Windows.Forms.DateTimePicker();
            this.HeureArrivePicker = new System.Windows.Forms.DateTimePicker();
            this.DateVisitePicker = new System.Windows.Forms.DateTimePicker();
            this.QuelMedecinComboBox = new System.Windows.Forms.ComboBox();
            this.RdvCheckbox = new System.Windows.Forms.CheckBox();
            this.HeureDepartLabel = new System.Windows.Forms.Label();
            this.QuelMedecinLabel = new System.Windows.Forms.Label();
            this.HeureArriveLabel = new System.Windows.Forms.Label();
            this.rdvLabel = new System.Windows.Forms.Label();
            this.AjoutVisiteBtn = new System.Windows.Forms.Button();
            this.DateVisiteLabel = new System.Windows.Forms.Label();
            this.ReadDeleteVisite = new System.Windows.Forms.TabPage();
            this.DatagridviewVisite = new System.Windows.Forms.DataGridView();
            this.Supprimer = new System.Windows.Forms.DataGridViewButtonColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.Statistiques = new System.Windows.Forms.TabPage();
            this.StatsQuelJour_Labl = new System.Windows.Forms.Label();
            this.StatsChoixJour_Picker = new System.Windows.Forms.DateTimePicker();
            this.panel2 = new System.Windows.Forms.Panel();
            this.TempsAttenteJour_Content = new System.Windows.Forms.Label();
            this.TempsChaqueVis_BTN = new System.Windows.Forms.Button();
            this.TempsChaqueVis_Label = new System.Windows.Forms.Label();
            this.VisiteJour_Content = new System.Windows.Forms.Label();
            this.TempsAttenteJour_Label = new System.Windows.Forms.Label();
            this.VisiteJour_Label = new System.Windows.Forms.Label();
            this.TempsAttTotal_Content = new System.Windows.Forms.Label();
            this.TempsAttTotal_Label = new System.Windows.Forms.Label();
            this.StatMain_Label = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.MenuVisite.SuspendLayout();
            this.AjouterVisite.SuspendLayout();
            this.ReadDeleteVisite.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DatagridviewVisite)).BeginInit();
            this.Statistiques.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.disconnectBTN);
            this.groupBox2.Controls.Add(this.MenuVisite);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(617, 390);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Vos visites";
            // 
            // disconnectBTN
            // 
            this.disconnectBTN.Location = new System.Drawing.Point(521, 0);
            this.disconnectBTN.Name = "disconnectBTN";
            this.disconnectBTN.Size = new System.Drawing.Size(81, 23);
            this.disconnectBTN.TabIndex = 30;
            this.disconnectBTN.Text = "Déconnexion";
            this.disconnectBTN.UseVisualStyleBackColor = true;
            this.disconnectBTN.Click += new System.EventHandler(this.disconnectBTN_Click);
            // 
            // MenuVisite
            // 
            this.MenuVisite.Controls.Add(this.AjouterVisite);
            this.MenuVisite.Controls.Add(this.ReadDeleteVisite);
            this.MenuVisite.Controls.Add(this.Statistiques);
            this.MenuVisite.Location = new System.Drawing.Point(6, 19);
            this.MenuVisite.Name = "MenuVisite";
            this.MenuVisite.SelectedIndex = 0;
            this.MenuVisite.Size = new System.Drawing.Size(600, 361);
            this.MenuVisite.TabIndex = 0;
            // 
            // AjouterVisite
            // 
            this.AjouterVisite.Controls.Add(this.AddVisiteHeaderLabel);
            this.AjouterVisite.Controls.Add(this.HeureDepartPicker);
            this.AjouterVisite.Controls.Add(this.HeureArrivePicker);
            this.AjouterVisite.Controls.Add(this.DateVisitePicker);
            this.AjouterVisite.Controls.Add(this.QuelMedecinComboBox);
            this.AjouterVisite.Controls.Add(this.RdvCheckbox);
            this.AjouterVisite.Controls.Add(this.HeureDepartLabel);
            this.AjouterVisite.Controls.Add(this.QuelMedecinLabel);
            this.AjouterVisite.Controls.Add(this.HeureArriveLabel);
            this.AjouterVisite.Controls.Add(this.rdvLabel);
            this.AjouterVisite.Controls.Add(this.AjoutVisiteBtn);
            this.AjouterVisite.Controls.Add(this.DateVisiteLabel);
            this.AjouterVisite.Location = new System.Drawing.Point(4, 22);
            this.AjouterVisite.Name = "AjouterVisite";
            this.AjouterVisite.Padding = new System.Windows.Forms.Padding(3);
            this.AjouterVisite.Size = new System.Drawing.Size(592, 335);
            this.AjouterVisite.TabIndex = 0;
            this.AjouterVisite.Text = "Ajouter Visite";
            this.AjouterVisite.UseVisualStyleBackColor = true;
            // 
            // AddVisiteHeaderLabel
            // 
            this.AddVisiteHeaderLabel.AutoSize = true;
            this.AddVisiteHeaderLabel.Location = new System.Drawing.Point(248, 32);
            this.AddVisiteHeaderLabel.Name = "AddVisiteHeaderLabel";
            this.AddVisiteHeaderLabel.Size = new System.Drawing.Size(88, 13);
            this.AddVisiteHeaderLabel.TabIndex = 72;
            this.AddVisiteHeaderLabel.Text = "Ajouter une visite";
            // 
            // HeureDepartPicker
            // 
            this.HeureDepartPicker.CustomFormat = "\'MMMM\'";
            this.HeureDepartPicker.Location = new System.Drawing.Point(319, 170);
            this.HeureDepartPicker.Name = "HeureDepartPicker";
            this.HeureDepartPicker.Size = new System.Drawing.Size(100, 20);
            this.HeureDepartPicker.TabIndex = 71;
            // 
            // HeureArrivePicker
            // 
            this.HeureArrivePicker.CustomFormat = "\'MMMM\'";
            this.HeureArrivePicker.Location = new System.Drawing.Point(319, 144);
            this.HeureArrivePicker.Name = "HeureArrivePicker";
            this.HeureArrivePicker.Size = new System.Drawing.Size(100, 20);
            this.HeureArrivePicker.TabIndex = 70;
            // 
            // DateVisitePicker
            // 
            this.DateVisitePicker.CustomFormat = "\'MMMM\'";
            this.DateVisitePicker.Location = new System.Drawing.Point(319, 92);
            this.DateVisitePicker.Name = "DateVisitePicker";
            this.DateVisitePicker.Size = new System.Drawing.Size(100, 20);
            this.DateVisitePicker.TabIndex = 69;
            // 
            // QuelMedecinComboBox
            // 
            this.QuelMedecinComboBox.FormattingEnabled = true;
            this.QuelMedecinComboBox.Location = new System.Drawing.Point(319, 196);
            this.QuelMedecinComboBox.MaxLength = 255;
            this.QuelMedecinComboBox.Name = "QuelMedecinComboBox";
            this.QuelMedecinComboBox.Size = new System.Drawing.Size(160, 21);
            this.QuelMedecinComboBox.TabIndex = 68;
            // 
            // RdvCheckbox
            // 
            this.RdvCheckbox.AutoSize = true;
            this.RdvCheckbox.Location = new System.Drawing.Point(319, 118);
            this.RdvCheckbox.Name = "RdvCheckbox";
            this.RdvCheckbox.Size = new System.Drawing.Size(46, 17);
            this.RdvCheckbox.TabIndex = 67;
            this.RdvCheckbox.Text = "Non";
            this.RdvCheckbox.UseVisualStyleBackColor = true;
            this.RdvCheckbox.CheckedChanged += new System.EventHandler(this.RdvCheckbox_CheckedChanged);
            // 
            // HeureDepartLabel
            // 
            this.HeureDepartLabel.AutoSize = true;
            this.HeureDepartLabel.Location = new System.Drawing.Point(155, 170);
            this.HeureDepartLabel.Name = "HeureDepartLabel";
            this.HeureDepartLabel.Size = new System.Drawing.Size(90, 13);
            this.HeureDepartLabel.TabIndex = 66;
            this.HeureDepartLabel.Text = "Heure de départ :";
            // 
            // QuelMedecinLabel
            // 
            this.QuelMedecinLabel.AutoSize = true;
            this.QuelMedecinLabel.Location = new System.Drawing.Point(154, 196);
            this.QuelMedecinLabel.Name = "QuelMedecinLabel";
            this.QuelMedecinLabel.Size = new System.Drawing.Size(69, 13);
            this.QuelMedecinLabel.TabIndex = 65;
            this.QuelMedecinLabel.Text = "Médecin vu :";
            // 
            // HeureArriveLabel
            // 
            this.HeureArriveLabel.AutoSize = true;
            this.HeureArriveLabel.Location = new System.Drawing.Point(154, 144);
            this.HeureArriveLabel.Name = "HeureArriveLabel";
            this.HeureArriveLabel.Size = new System.Drawing.Size(79, 13);
            this.HeureArriveLabel.TabIndex = 64;
            this.HeureArriveLabel.Text = "Heure d\'arrivé :";
            // 
            // rdvLabel
            // 
            this.rdvLabel.AutoSize = true;
            this.rdvLabel.Location = new System.Drawing.Point(154, 118);
            this.rdvLabel.Name = "rdvLabel";
            this.rdvLabel.Size = new System.Drawing.Size(90, 13);
            this.rdvLabel.TabIndex = 63;
            this.rdvLabel.Text = "Sur rendez-vous?";
            // 
            // AjoutVisiteBtn
            // 
            this.AjoutVisiteBtn.Location = new System.Drawing.Point(319, 233);
            this.AjoutVisiteBtn.Name = "AjoutVisiteBtn";
            this.AjoutVisiteBtn.Size = new System.Drawing.Size(100, 23);
            this.AjoutVisiteBtn.TabIndex = 56;
            this.AjoutVisiteBtn.Text = "Ajouter";
            this.AjoutVisiteBtn.UseVisualStyleBackColor = true;
            this.AjoutVisiteBtn.Click += new System.EventHandler(this.AjoutVisiteBtn_Click);
            // 
            // DateVisiteLabel
            // 
            this.DateVisiteLabel.AutoSize = true;
            this.DateVisiteLabel.Location = new System.Drawing.Point(154, 92);
            this.DateVisiteLabel.Name = "DateVisiteLabel";
            this.DateVisiteLabel.Size = new System.Drawing.Size(89, 13);
            this.DateVisiteLabel.TabIndex = 59;
            this.DateVisiteLabel.Text = "Date de la visite :";
            // 
            // ReadDeleteVisite
            // 
            this.ReadDeleteVisite.Controls.Add(this.DatagridviewVisite);
            this.ReadDeleteVisite.Controls.Add(this.label1);
            this.ReadDeleteVisite.Location = new System.Drawing.Point(4, 22);
            this.ReadDeleteVisite.Name = "ReadDeleteVisite";
            this.ReadDeleteVisite.Padding = new System.Windows.Forms.Padding(3);
            this.ReadDeleteVisite.Size = new System.Drawing.Size(592, 335);
            this.ReadDeleteVisite.TabIndex = 1;
            this.ReadDeleteVisite.Text = "Voir / Supprimer vos Visites";
            this.ReadDeleteVisite.UseVisualStyleBackColor = true;
            // 
            // DatagridviewVisite
            // 
            this.DatagridviewVisite.AllowUserToAddRows = false;
            this.DatagridviewVisite.AllowUserToOrderColumns = true;
            this.DatagridviewVisite.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.DatagridviewVisite.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.DatagridviewVisite.BackgroundColor = System.Drawing.Color.White;
            this.DatagridviewVisite.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DatagridviewVisite.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Supprimer});
            this.DatagridviewVisite.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DatagridviewVisite.Location = new System.Drawing.Point(3, 3);
            this.DatagridviewVisite.Name = "DatagridviewVisite";
            this.DatagridviewVisite.RowHeadersVisible = false;
            this.DatagridviewVisite.Size = new System.Drawing.Size(586, 329);
            this.DatagridviewVisite.TabIndex = 2;
            this.DatagridviewVisite.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewVisites_CellContentClick);
            // 
            // Supprimer
            // 
            this.Supprimer.HeaderText = "Supprimer";
            this.Supprimer.Name = "Supprimer";
            this.Supprimer.Text = "Supprimer";
            this.Supprimer.ToolTipText = "Supprimer";
            this.Supprimer.UseColumnTextForButtonValue = true;
            this.Supprimer.Width = 60;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(185, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Fonctionnalité à venir!";
            // 
            // Statistiques
            // 
            this.Statistiques.Controls.Add(this.StatsQuelJour_Labl);
            this.Statistiques.Controls.Add(this.StatsChoixJour_Picker);
            this.Statistiques.Controls.Add(this.panel2);
            this.Statistiques.Controls.Add(this.StatMain_Label);
            this.Statistiques.Location = new System.Drawing.Point(4, 22);
            this.Statistiques.Name = "Statistiques";
            this.Statistiques.Padding = new System.Windows.Forms.Padding(3);
            this.Statistiques.Size = new System.Drawing.Size(592, 335);
            this.Statistiques.TabIndex = 2;
            this.Statistiques.Text = "Vos statistiques";
            this.Statistiques.UseVisualStyleBackColor = true;
            // 
            // StatsQuelJour_Labl
            // 
            this.StatsQuelJour_Labl.AutoSize = true;
            this.StatsQuelJour_Labl.Location = new System.Drawing.Point(159, 96);
            this.StatsQuelJour_Labl.Name = "StatsQuelJour_Labl";
            this.StatsQuelJour_Labl.Size = new System.Drawing.Size(97, 13);
            this.StatsQuelJour_Labl.TabIndex = 23;
            this.StatsQuelJour_Labl.Text = "Choisissez un jour :";
            // 
            // StatsChoixJour_Picker
            // 
            this.StatsChoixJour_Picker.Location = new System.Drawing.Point(262, 90);
            this.StatsChoixJour_Picker.Name = "StatsChoixJour_Picker";
            this.StatsChoixJour_Picker.Size = new System.Drawing.Size(151, 20);
            this.StatsChoixJour_Picker.TabIndex = 22;
            this.StatsChoixJour_Picker.ValueChanged += new System.EventHandler(this.StatsChoixJour_Picker_ValueChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TempsAttenteJour_Content);
            this.panel2.Controls.Add(this.TempsChaqueVis_BTN);
            this.panel2.Controls.Add(this.TempsChaqueVis_Label);
            this.panel2.Controls.Add(this.VisiteJour_Content);
            this.panel2.Controls.Add(this.TempsAttenteJour_Label);
            this.panel2.Controls.Add(this.VisiteJour_Label);
            this.panel2.Controls.Add(this.TempsAttTotal_Content);
            this.panel2.Controls.Add(this.TempsAttTotal_Label);
            this.panel2.Location = new System.Drawing.Point(26, 141);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(544, 175);
            this.panel2.TabIndex = 19;
            // 
            // TempsAttenteJour_Content
            // 
            this.TempsAttenteJour_Content.AutoSize = true;
            this.TempsAttenteJour_Content.Location = new System.Drawing.Point(161, 122);
            this.TempsAttenteJour_Content.Name = "TempsAttenteJour_Content";
            this.TempsAttenteJour_Content.Size = new System.Drawing.Size(10, 13);
            this.TempsAttenteJour_Content.TabIndex = 22;
            this.TempsAttenteJour_Content.Text = "-";
            // 
            // TempsChaqueVis_BTN
            // 
            this.TempsChaqueVis_BTN.Location = new System.Drawing.Point(427, 75);
            this.TempsChaqueVis_BTN.Name = "TempsChaqueVis_BTN";
            this.TempsChaqueVis_BTN.Size = new System.Drawing.Size(52, 23);
            this.TempsChaqueVis_BTN.TabIndex = 11;
            this.TempsChaqueVis_BTN.Text = "Voir";
            this.TempsChaqueVis_BTN.UseVisualStyleBackColor = true;
            this.TempsChaqueVis_BTN.Click += new System.EventHandler(this.TempsChaqueVis_BTN_Click);
            // 
            // TempsChaqueVis_Label
            // 
            this.TempsChaqueVis_Label.AutoSize = true;
            this.TempsChaqueVis_Label.Location = new System.Drawing.Point(327, 80);
            this.TempsChaqueVis_Label.Name = "TempsChaqueVis_Label";
            this.TempsChaqueVis_Label.Size = new System.Drawing.Size(94, 13);
            this.TempsChaqueVis_Label.TabIndex = 16;
            this.TempsChaqueVis_Label.Text = "Toutes mes visites";
            // 
            // VisiteJour_Content
            // 
            this.VisiteJour_Content.AutoSize = true;
            this.VisiteJour_Content.Location = new System.Drawing.Point(161, 80);
            this.VisiteJour_Content.Name = "VisiteJour_Content";
            this.VisiteJour_Content.Size = new System.Drawing.Size(10, 13);
            this.VisiteJour_Content.TabIndex = 21;
            this.VisiteJour_Content.Text = "-";
            // 
            // TempsAttenteJour_Label
            // 
            this.TempsAttenteJour_Label.AutoSize = true;
            this.TempsAttenteJour_Label.Location = new System.Drawing.Point(40, 122);
            this.TempsAttenteJour_Label.Name = "TempsAttenteJour_Label";
            this.TempsAttenteJour_Label.Size = new System.Drawing.Size(124, 13);
            this.TempsAttenteJour_Label.TabIndex = 21;
            this.TempsAttenteJour_Label.Text = "Temps d\'attente ce jour :";
            // 
            // VisiteJour_Label
            // 
            this.VisiteJour_Label.AutoSize = true;
            this.VisiteJour_Label.Location = new System.Drawing.Point(40, 80);
            this.VisiteJour_Label.Name = "VisiteJour_Label";
            this.VisiteJour_Label.Size = new System.Drawing.Size(131, 13);
            this.VisiteJour_Label.TabIndex = 20;
            this.VisiteJour_Label.Text = "Visites effectuées ce jour :";
            // 
            // TempsAttTotal_Content
            // 
            this.TempsAttTotal_Content.AutoSize = true;
            this.TempsAttTotal_Content.Location = new System.Drawing.Point(233, 44);
            this.TempsAttTotal_Content.Name = "TempsAttTotal_Content";
            this.TempsAttTotal_Content.Size = new System.Drawing.Size(10, 13);
            this.TempsAttTotal_Content.TabIndex = 19;
            this.TempsAttTotal_Content.Text = "-";
            // 
            // TempsAttTotal_Label
            // 
            this.TempsAttTotal_Label.AutoSize = true;
            this.TempsAttTotal_Label.Location = new System.Drawing.Point(220, 21);
            this.TempsAttTotal_Label.Name = "TempsAttTotal_Label";
            this.TempsAttTotal_Label.Size = new System.Drawing.Size(119, 13);
            this.TempsAttTotal_Label.TabIndex = 0;
            this.TempsAttTotal_Label.Text = "Temps d\'attente Total : ";
            // 
            // StatMain_Label
            // 
            this.StatMain_Label.AutoSize = true;
            this.StatMain_Label.Location = new System.Drawing.Point(205, 41);
            this.StatMain_Label.Name = "StatMain_Label";
            this.StatMain_Label.Size = new System.Drawing.Size(88, 13);
            this.StatMain_Label.TabIndex = 3;
            this.StatMain_Label.Text = "Statistiques de  : ";
            // 
            // FormVisiteur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(641, 409);
            this.Controls.Add(this.groupBox2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormVisiteur";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Espace Visiteur";
            this.Load += new System.EventHandler(this.FormVisiteur_Load);
            this.groupBox2.ResumeLayout(false);
            this.MenuVisite.ResumeLayout(false);
            this.AjouterVisite.ResumeLayout(false);
            this.AjouterVisite.PerformLayout();
            this.ReadDeleteVisite.ResumeLayout(false);
            this.ReadDeleteVisite.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DatagridviewVisite)).EndInit();
            this.Statistiques.ResumeLayout(false);
            this.Statistiques.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button disconnectBTN;
        private System.Windows.Forms.TabControl MenuVisite;
        private System.Windows.Forms.TabPage AjouterVisite;
        private System.Windows.Forms.DateTimePicker HeureDepartPicker;
        private System.Windows.Forms.DateTimePicker HeureArrivePicker;
        private System.Windows.Forms.DateTimePicker DateVisitePicker;
        private System.Windows.Forms.ComboBox QuelMedecinComboBox;
        private System.Windows.Forms.CheckBox RdvCheckbox;
        private System.Windows.Forms.Label HeureDepartLabel;
        private System.Windows.Forms.Label QuelMedecinLabel;
        private System.Windows.Forms.Label HeureArriveLabel;
        private System.Windows.Forms.Label rdvLabel;
        private System.Windows.Forms.Button AjoutVisiteBtn;
        private System.Windows.Forms.Label DateVisiteLabel;
        private System.Windows.Forms.TabPage ReadDeleteVisite;
        private System.Windows.Forms.DataGridView DatagridviewVisite;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage Statistiques;
        private System.Windows.Forms.Label AddVisiteHeaderLabel;
        private System.Windows.Forms.Label StatMain_Label;
        private System.Windows.Forms.DataGridViewButtonColumn Supprimer;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label TempsAttenteJour_Content;
        private System.Windows.Forms.Button TempsChaqueVis_BTN;
        private System.Windows.Forms.Label TempsChaqueVis_Label;
        private System.Windows.Forms.Label VisiteJour_Content;
        private System.Windows.Forms.Label TempsAttenteJour_Label;
        private System.Windows.Forms.Label VisiteJour_Label;
        private System.Windows.Forms.Label TempsAttTotal_Content;
        private System.Windows.Forms.Label TempsAttTotal_Label;
        private System.Windows.Forms.Label StatsQuelJour_Labl;
        private System.Windows.Forms.DateTimePicker StatsChoixJour_Picker;
    }
}