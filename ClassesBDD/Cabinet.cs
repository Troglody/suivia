﻿using JsonFx.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuiviA
{
    public class Cabinet
    {
        [JsonName("idCabinet")]
        public int idCabinet { get; set; }

        [JsonName("ville")]
        public string ville { get; set; }

        [JsonName("adresse")]
        public string adresse { get; set; }

        [JsonName("gps")]
        public string gps { get; set; }


        public Cabinet() { }//GET


        public Cabinet(int _idCabinet, string _ville, string _adresse, string _gps)//PUT
        {
            this.idCabinet = _idCabinet;
            this.ville = _ville;
            this.adresse = _adresse;
            this.gps = _gps;

        }
        public Cabinet(string _ville, string _adresse, string _gps)//POST
        {
            this.ville = _ville;
            this.adresse = _adresse;
            this.gps = _gps;
        }
    }
}
