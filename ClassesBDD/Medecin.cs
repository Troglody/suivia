﻿using JsonFx.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuiviA
{
    public class Medecin
    {
        [JsonName("idMedecin")]
        public int idMedecin { get; set; }

        [JsonName("nom")]
        public string nom { get; set; }

        [JsonName("prenom")]
        public string prenom { get; set; }

        [JsonName("idcabinet")]
        public int idcabinet { get; set; }


        public Medecin() { } //GET

        public Medecin(string _nom, string _prenom, int _idcabinet)//PUT
        {
            this.nom = _nom;
            this.prenom = _prenom;
            this.idcabinet = _idcabinet;

        }
        public Medecin(int _idMedecin, string _nom, string _prenom, int _idcabinet)//POST
        {
            this.idMedecin = _idMedecin;
            this.nom = _nom;
            this.prenom = _prenom;
            this.idcabinet = _idcabinet;
        }
    }
}
