﻿using JsonFx.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuiviA
{

    public class Timetotal
    {

        [JsonName("sumtime")]
        public string sumtime { get; set; }
    }

}
