﻿
using JsonFx.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SuiviA
{
    public class Visite
    {
        [JsonName("idVisite")]
        public int idVisite { get; set; }
        [JsonName("dateV")]
        public string dateV { get; set; }
        [JsonName("rdv")]
        public string rdv { get; set; }
        [JsonName("heure_arrive")]
        public string heure_arrive { get; set; }
        [JsonName("heure_depart")]
        public string heure_depart { get; set; }
        [JsonName("idmedecin")]
        public int idmedecin { get; set; }


       public Visite() { } // GET

       public Visite(string _date_v, string _rdv, string _heure_arrive, string _heure_depart, int _idmedecin)
        {
            this.dateV = _date_v;
            this.rdv = _rdv;
            this.heure_arrive = _heure_arrive;
            this.heure_depart = _heure_depart;
            this.idmedecin = _idmedecin;
        } // POST PUT 

    }
}
