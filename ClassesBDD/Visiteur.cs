﻿using JsonFx.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuiviA
{
    public class Visiteur
    {
        [JsonName("id")]
        public string id { get; set; }
        [JsonName("nom")]
        public string nom { get; set; }
        [JsonName("prenom")]
        public string prenom { get; set; }
        [JsonName("login")]
        public string login { get; set; }
        [JsonName("mdp")]
        public string mdp { get; set; }
        [JsonName("adresse")]
        public string adresse { get; set; }
        [JsonName("cp")]
        public string cp { get; set; }
        [JsonName("ville")]
        public string ville { get; set; }
        [JsonName("dateEmbauche")]
        public DateTime dateEmbauche { get; set; }
        [JsonName("level")]
        public int level { get; set; }

        public Visiteur() { }

        public Visiteur(string _id, string _nom, string _prenom, string _login, string _mdp, string _adresse, string _cp, string _ville, DateTime _dateEmbauche, int _level)
        {
            this.id = _id;
            this.nom = _nom;
            this.prenom = _prenom;
            this.login = _login;
            this.mdp = _mdp;
            this.adresse = _adresse;
            this.cp = _cp;
            this.ville = _ville;
            this.dateEmbauche = _dateEmbauche;
            this.level = _level;
        }

    }
}
