﻿using JsonFx.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuiviA
{
    public class TimetotalJour
    {

        [JsonName("sumtime")]
        public string TempsTotalJour { get; set; }
    }
}
