﻿using JsonFx.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuiviA 
{

    public class Timediff
    {
        [JsonName("idVisite")]
        public long Visite { get; set; }

        [JsonName("dateV")]
        public string Date { get; set; }

        [JsonName("timediff")]
        public string Durée { get; set; }
    }

}
