﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Net.WebRequestMethods;
using EasyHttp.Http;


namespace SuiviA
{
    public partial class FormConnexion : Form
    {

        public FormConnexion()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            co_password.PasswordChar = '*';

            this.AcceptButton = ConnexionBTN; //Appuyer sur entrer = Appuyer sur connexion     
 
        }

        private void ConnexionBTN_Click(object sender, EventArgs e) //Connexion as visiteur ou admin en fonction du grade (level) de l'utilisateur enregistré en BDD
        {
             var connexion = false; 
             var niveau = false;
            //Alternatives à cannot "else" in foreach

            if (checkBox1.Checked)
            {
                try
                {
                    if (ActiveDirectory.ActiveDirectoryConnexion(co_username.Text, co_password.Text, "gsb.local"))
                    {
                        if (ActiveDirectory.UserType == "Visiteur")
                        {
                            this.Hide();
                            FormVisiteur f3 = new FormVisiteur();
                            f3.ShowDialog();
                        }

                        else if (ActiveDirectory.UserType == "Admin")
                        {
                            this.Hide();
                            FormAdministration f2 = new FormAdministration();
                            f2.ShowDialog();
                        }

                        else
                        {
                            MessageBox.Show("Impossible de trouver le niveau d'utilisateur !");
                        }
                    }

                    else
                    {
                        MessageBox.Show("Utilisateur introuvable !");
                    }
                }
                catch {  }
            }

            else
            {

                try
                {
                    foreach (var v in Service.getVisiteurs())
                    {
                        if (v.login == co_username.Text && v.mdp == co_password.Text)
                        {
                            User.id = v.id;
                            User.nom = v.nom;
                            User.prenom = v.prenom;


                            if (v.level == 1)
                            {
                                niveau = true;
                                this.Hide();
                                FormAdministration f2 = new FormAdministration();
                                f2.ShowDialog();
                            }

                            if (v.level == 0)
                            {
                                niveau = true;
                                this.Hide();
                                FormVisiteur f3 = new FormVisiteur();
                                f3.ShowDialog();
                            }
                            connexion = true;
                            break;
                        }
                    }
                    if (!connexion) { MessageBox.Show("Identifiants incorrect !"); }
                    if (!niveau) { MessageBox.Show("Niveau utilisateur insuffisant !"); }
                }
                catch { MessageBox.Show("Une erreur est survenue \n  -  Vérifiez la connexion au serveur local ou à la base de données", "Attention!"); }

            }

            
        }


    }
}

