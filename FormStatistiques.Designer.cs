﻿namespace SuiviA
{
    partial class FormStatistiques
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.WhatStat_Groupbox = new System.Windows.Forms.GroupBox();
            this.StatDataGridView = new System.Windows.Forms.DataGridView();
            this.WhatStat_Groupbox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StatDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // WhatStat_Groupbox
            // 
            this.WhatStat_Groupbox.Controls.Add(this.StatDataGridView);
            this.WhatStat_Groupbox.Location = new System.Drawing.Point(12, 12);
            this.WhatStat_Groupbox.Name = "WhatStat_Groupbox";
            this.WhatStat_Groupbox.Size = new System.Drawing.Size(260, 237);
            this.WhatStat_Groupbox.TabIndex = 0;
            this.WhatStat_Groupbox.TabStop = false;
            this.WhatStat_Groupbox.Text = "WhatStat";
            // 
            // StatDataGridView
            // 
            this.StatDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.StatDataGridView.Location = new System.Drawing.Point(14, 19);
            this.StatDataGridView.Name = "StatDataGridView";
            this.StatDataGridView.Size = new System.Drawing.Size(240, 212);
            this.StatDataGridView.TabIndex = 0;
            // 
            // FormStatistiques
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.WhatStat_Groupbox);
            this.Name = "FormStatistiques";
            this.Text = "FormStatistiques";
            this.Load += new System.EventHandler(this.FormStatistiques_Load);
            this.WhatStat_Groupbox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.StatDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox WhatStat_Groupbox;
        private System.Windows.Forms.DataGridView StatDataGridView;
    }
}