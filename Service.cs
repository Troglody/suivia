﻿using EasyHttp.Http;
using SuiviA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuiviA
{
    public static class Service
    {

          private static readonly string baseUrl = "http://localhost:7203/";
      
        private static readonly string googleKey = "AIzaSyADLNWIXwnwJ-mcgzLes_Og-FiYhWDcmxk";


        //public static List<Etat> getEtat()
        //{
        //    try
        //    {
        //        var http = new HttpClient();
        //        var response = http.Get(baseUrl + "api/etat");
        //        return response.StaticBody<List<Etat>>();
        //    }
        //    catch { return null; }
        //}



        public static List<Visite> getVisiteID(string id)
        {
            try
            {
                var Http = new HttpClient();
                Http.Request.Accept = HttpContentTypes.ApplicationJson;
                var response = Http.Get(baseUrl + "api/visite/" + id);
                return response.StaticBody<List<Visite>>(); // Faire une nouvelle classe avec les bons champs
            }
            catch { return null; }
        }

        // select visite.idVisite, visite.dateV, visite.rdv, visite.heure_arrive,visite.heure_depart,visite.idmedecin FROM visite JOIN medecin ON visite.idmedecin = medecin.idMedecin JOIN visiteur ON medecin.idVisiteur = visiteur.id WHERE medecin.idVisiteur = ? ORDER BY idVisite'[req.params.id] 


        public static List<Visite> getVisiteAll()
        {
            try
            {
                var Http = new HttpClient();
                Http.Request.Accept = HttpContentTypes.ApplicationJson;
                var response = Http.Get(baseUrl + "api/visite");
                return response.StaticBody<List<Visite>>();
            }
            catch { return null; }
        }



        public static List<Visiteur> getVisiteurs()
        {
            try
            {
                var Http = new HttpClient();
                Http.Request.Accept = HttpContentTypes.ApplicationJson;
                var response = Http.Get(baseUrl + "api/visiteur");
                return response.StaticBody<List<Visiteur>>();
            }
            catch { return null; }
        }


        public static List<Medecin> getMedecin()
        {
            try
            {
                var Http = new HttpClient();
                Http.Request.Accept = HttpContentTypes.ApplicationJson;
                var response = Http.Get(baseUrl + "api/medecin");
                return response.StaticBody<List<Medecin>>();
            }
            catch { return null; }
        }

        public static List<Cabinet> getCabinet()
        {
            try
            {
                var Http = new HttpClient();
                Http.Request.Accept = HttpContentTypes.ApplicationJson;
                var response = Http.Get(baseUrl + "api/cabinet");
                return response.StaticBody<List<Cabinet>>();
            }
            catch { return null; }
        }



        public static bool put(string api, object data)
        {
            try
            {
                var Http = new HttpClient();
                var response = Http.Put(baseUrl + api, data, HttpContentTypes.ApplicationJson);
                return response.StatusCode.ToString() == "OK" ? true : false;
            }
            catch { return false; }
        }


        public static bool post(string api, object data)
        {
            try
            {
                var Http = new HttpClient();
                var response = Http.Post(baseUrl + api, data, HttpContentTypes.ApplicationJson);
                return response.StatusCode.ToString() == "OK" ? true : false;
            }
            catch { return false; }
        }



        public static bool supprimervisite(string api)
        {
            try
            {
                var http = new HttpClient();
                var response = http.Delete(baseUrl + api);
                return response.StatusCode.ToString() == "OK" ? true : false;
            }
            catch { return false; }
        }


 
        public static List<Timediff> GetTimeDiff(string id)
        {
            try
            {
                var Http = new HttpClient();
                Http.Request.Accept = HttpContentTypes.ApplicationJson;
                var response = Http.Get(baseUrl + "api/timediff/" + id);
                return response.StaticBody<List<Timediff>>();

            }
            catch { return null; }
        }


        public static List<Timetotal> GetTotalWait(string id)
        {
            try
            {
                var Http = new HttpClient();
                Http.Request.Accept = HttpContentTypes.ApplicationJson;
                var response = Http.Get(baseUrl + "api/totalwait/" + id);
                return response.StaticBody<List<Timetotal>>();

            }
            catch { return null; }
        }


        public static List<TimetotalJour> GetTimeTotalJour(string id,string date)
        {
            try
            {
                var Http = new HttpClient();
                Http.Request.Accept = HttpContentTypes.ApplicationJson;
                var response = Http.Get(baseUrl + "api/totaltimejour/" + id + "/" + date);
                return response.StaticBody<List<TimetotalJour>>();
            }
            catch { return null; }
        }


        public static List<TimetotalJour> getNbrVisiteparJour(string id, string date)
        {
            try
            {
                var Http = new HttpClient();
                Http.Request.Accept = HttpContentTypes.ApplicationJson;
                var response = Http.Get(baseUrl + "api/NbrVisiteparJour/" + id + "/" + date);
                return response.StaticBody<List<TimetotalJour>>();
            }
            catch { return null; }
 
        }

        public static void AffectVisiteur(object dataIdVisiteur,int idMedecin)
        {
            try
            {
                var Http = new HttpClient();
                Http.Request.Accept = HttpContentTypes.ApplicationJson;
                var response = Http.Put(baseUrl + "api/affectvisiteur/" + idMedecin.ToString(), dataIdVisiteur, HttpContentTypes.ApplicationJson);
            }
            catch {   }

        }


        public static Gps getGps(string codepostal)
        {

            try
            {
                var Http = new HttpClient();
                Http.Request.Accept = HttpContentTypes.ApplicationJson;
                var response = Http.Get("https://maps.googleapis.com/maps/api/geocode/json?address=" + codepostal + "&region=fr&key=" + googleKey);
                return response.StaticBody<Gps>();

            }
            catch { return null; }
        }

    }
}

 