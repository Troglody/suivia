﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SuiviA
{
    public static class ActiveDirectory
    {
        public static string UserType { get; set; }
        /// <summary>
        /// Connexion à l'active directory 
        /// </summary>
        /// <param name="username">Login à l'active directory</param>
        /// <param name="password">Mot de passe de l'utilisateur pour se connecter à l'active directory</param>
        /// <param name="domain">Nom du domaine de l'active directory</param>
        /// <returns>True: Username/Password OK; False: Authentication error</returns>
        public static bool ActiveDirectoryConnexion(string username, string password, string domain)
        {
            try
            {
                DirectoryEntry ActiveDirectoryConnexionPath = new DirectoryEntry("LDAP://" + domain, username, password, AuthenticationTypes.Secure);
                DirectorySearcher Search = new DirectorySearcher(ActiveDirectoryConnexionPath);
                Search.PropertiesToLoad.Add("cn");
                Search.PropertiesToLoad.Add("givenname");
                Search.PropertiesToLoad.Add("sn");
                Search.PropertiesToLoad.Add("memberof");
                Search.Filter = "(&(objectClass=user)(SAMAccountName=" + username + "))";
                SearchResult ResultSearch = Search.FindOne();
                return GetGroup(ResultSearch);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Connexion échoué !");
                return false;
            }
        }

        /// <summary>
        /// Cette méthode permet de chercher l'appartenance à un groupe de l'utilisateur qui s'est connecté à l'active directory.
        /// </summary>
        /// <param name="ResultSearch">Objet SearchResult qui prend en paramètre le nom de l'utilsateur qui s'est connecté à l'active directory</param>
        /// <returns>True: l'utilisateur appartient au groupe Admin ou Visiteur et enregistre le résultat dans la variable UserType | False : l'utilisateur n'appartient à aucun des deux groupes</returns>
        private static bool GetGroup(SearchResult ResultSearch)
        {
            if (ResultSearch != null)
            {
                if (ResultSearch.Properties["memberof"] != null)
                {
                    ResultPropertyValueCollection groups = ResultSearch.Properties["memberof"];

                    foreach (string group in groups)
                    {
                        if (group.Contains("CN=" + "UserNode"))
                        {
                            UserType = "Visiteur";
                            return true;
                        }
                        if (group.Contains("CN=" + "AdminNode"))
                        {
                            UserType = "Admin";
                            return true;
                        }
                    }
                }
            }
            MessageBox.Show("Utilisateur Introuvable");
            return false; //r.haubert  //AzertY!59000
        }
    }
}
