﻿using JsonFx.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuiviA
{
    public class Gps
    {
        [JsonName("results")]
        public Result[] Results { get; set; }

        [JsonName("status")]
        public string Status { get; set; }
    }

    public class Result
    {
        [JsonName("address_components")]
        public AddressComponent[] AddressComponents { get; set; }

        [JsonName("formatted_address")]
        public string FormattedAddress { get; set; }

        [JsonName("geometry")]
        public Geometry Geometry { get; set; }

        [JsonName("place_id")]
        public string PlaceId { get; set; }

        [JsonName("types")]
        public string[] Types { get; set; }
    }

    public class AddressComponent
    {
        [JsonName("long_name")]
        public string LongName { get; set; }

        [JsonName("short_name")]
        public string ShortName { get; set; }

        [JsonName("types")]
        public string[] Types { get; set; }
    }

    public  class Geometry
    {
        [JsonName("bounds")]
        public Bounds Bounds { get; set; }

        [JsonName("location")]
        public Location Location { get; set; }

        [JsonName("location_type")]
        public string LocationType { get; set; }

        [JsonName("viewport")]
        public Bounds Viewport { get; set; }
    }

    public  class Bounds
    {
        [JsonName("northeast")]
        public Location Northeast { get; set; }

        [JsonName("southwest")]
        public Location Southwest { get; set; }
    }

    public  class Location
    {
        [JsonName("lat")]
        public double Lat { get; set; }

        [JsonName("lng")]
        public double Lng { get; set; }
    }
}
