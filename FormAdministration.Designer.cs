﻿namespace SuiviA
{
    partial class FormAdministration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAdministration));
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.ModifyTab = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.ModMed_QuelCabCombobox = new System.Windows.Forms.ComboBox();
            this.ModMed_BTN = new System.Windows.Forms.Button();
            this.ModMedCab_Label = new System.Windows.Forms.Label();
            this.ModMedPrenom_Label = new System.Windows.Forms.Label();
            this.ModMedNom_Label = new System.Windows.Forms.Label();
            this.ModMedPrenom_Textbox = new System.Windows.Forms.TextBox();
            this.ModMedNom_Textbox = new System.Windows.Forms.TextBox();
            this.ModMed_QuelMedTextbox = new System.Windows.Forms.Label();
            this.ModMed_QuelMedCombobox = new System.Windows.Forms.ComboBox();
            this.ModCabCab_Label = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.ModCabQuelCab_Combobox = new System.Windows.Forms.ComboBox();
            this.ModCab_BTN = new System.Windows.Forms.Button();
            this.ModCabGPS_Label = new System.Windows.Forms.Label();
            this.ModCabAdresse_Label = new System.Windows.Forms.Label();
            this.ModCabVille_Label = new System.Windows.Forms.Label();
            this.ModCabGPS_Textbox = new System.Windows.Forms.TextBox();
            this.ModCabVille_Textbox = new System.Windows.Forms.TextBox();
            this.ModCabAdresse_Textbox = new System.Windows.Forms.TextBox();
            this.AjouterTab = new System.Windows.Forms.TabPage();
            this.AddMenu = new System.Windows.Forms.TabControl();
            this.AddMedTab = new System.Windows.Forms.TabPage();
            this.AddMed_QuelCabCombobox = new System.Windows.Forms.ComboBox();
            this.AddMedBTN = new System.Windows.Forms.Button();
            this.AddMedCab_Label = new System.Windows.Forms.Label();
            this.AddMedPrenom_Label = new System.Windows.Forms.Label();
            this.AddMedNom_Label = new System.Windows.Forms.Label();
            this.medecin_prenom = new System.Windows.Forms.TextBox();
            this.medecin_nom = new System.Windows.Forms.TextBox();
            this.AddCabTab = new System.Windows.Forms.TabPage();
            this.AddCabVille_Textbox = new System.Windows.Forms.TextBox();
            this.AddCabBTN = new System.Windows.Forms.Button();
            this.AddCabVille_Label = new System.Windows.Forms.Label();
            this.AdminMenu = new System.Windows.Forms.TabControl();
            this.VtoMedTab = new System.Windows.Forms.TabPage();
            this.Affect_BTN = new System.Windows.Forms.Button();
            this.AffectMedecin_Label = new System.Windows.Forms.Label();
            this.AffectVisiteur_Label = new System.Windows.Forms.Label();
            this.AffectMedecin_ComboBox = new System.Windows.Forms.ComboBox();
            this.AffectVisiteur_ComboBox = new System.Windows.Forms.ComboBox();
            this.StatsQuelJour_Label = new System.Windows.Forms.TabPage();
            this.StatsQuelJour_Labl = new System.Windows.Forms.Label();
            this.StatsChoixJour_Picker = new System.Windows.Forms.DateTimePicker();
            this.panel2 = new System.Windows.Forms.Panel();
            this.TempsAttenteJour_Content = new System.Windows.Forms.Label();
            this.TempsChaqueVis_BTN = new System.Windows.Forms.Button();
            this.TempsChaqueVis_Label = new System.Windows.Forms.Label();
            this.VisiteJour_Content = new System.Windows.Forms.Label();
            this.TempsAttenteJour_Label = new System.Windows.Forms.Label();
            this.VisiteJour_Label = new System.Windows.Forms.Label();
            this.TempsAttTotal_Content = new System.Windows.Forms.Label();
            this.TempsAttTotal_Label = new System.Windows.Forms.Label();
            this.StatsQuelVisiteur_Label = new System.Windows.Forms.Label();
            this.StatsQuelVisiteur_Combobox = new System.Windows.Forms.ComboBox();
            this.disconnectBTN = new System.Windows.Forms.Button();
            this.SupprVisiteTab = new System.Windows.Forms.TabPage();
            this.Visites = new System.Windows.Forms.TabControl();
            this.AllVisites_Tab = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.DatagridviewVisite = new System.Windows.Forms.DataGridView();
            this.Supprimer = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewVisiteChoix = new System.Windows.Forms.DataGridView();
            this.dataGridViewButtonColumn1 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.VisiteQuelVisiteur_Label = new System.Windows.Forms.Label();
            this.VisiteQuelVisiteur_Combobox = new System.Windows.Forms.ComboBox();
            this.tabPage2.SuspendLayout();
            this.ModifyTab.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.ModCabCab_Label.SuspendLayout();
            this.AjouterTab.SuspendLayout();
            this.AddMenu.SuspendLayout();
            this.AddMedTab.SuspendLayout();
            this.AddCabTab.SuspendLayout();
            this.AdminMenu.SuspendLayout();
            this.VtoMedTab.SuspendLayout();
            this.StatsQuelJour_Label.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SupprVisiteTab.SuspendLayout();
            this.Visites.SuspendLayout();
            this.AllVisites_Tab.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DatagridviewVisite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewVisiteChoix)).BeginInit();
            this.SuspendLayout();
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.ModifyTab);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(607, 343);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Modifier";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // ModifyTab
            // 
            this.ModifyTab.Controls.Add(this.tabPage1);
            this.ModifyTab.Controls.Add(this.ModCabCab_Label);
            this.ModifyTab.Location = new System.Drawing.Point(6, 6);
            this.ModifyTab.Name = "ModifyTab";
            this.ModifyTab.SelectedIndex = 0;
            this.ModifyTab.Size = new System.Drawing.Size(595, 335);
            this.ModifyTab.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.ModMed_QuelCabCombobox);
            this.tabPage1.Controls.Add(this.ModMed_BTN);
            this.tabPage1.Controls.Add(this.ModMedCab_Label);
            this.tabPage1.Controls.Add(this.ModMedPrenom_Label);
            this.tabPage1.Controls.Add(this.ModMedNom_Label);
            this.tabPage1.Controls.Add(this.ModMedPrenom_Textbox);
            this.tabPage1.Controls.Add(this.ModMedNom_Textbox);
            this.tabPage1.Controls.Add(this.ModMed_QuelMedTextbox);
            this.tabPage1.Controls.Add(this.ModMed_QuelMedCombobox);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(587, 309);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Modifier médecin";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // ModMed_QuelCabCombobox
            // 
            this.ModMed_QuelCabCombobox.FormattingEnabled = true;
            this.ModMed_QuelCabCombobox.Location = new System.Drawing.Point(290, 151);
            this.ModMed_QuelCabCombobox.Name = "ModMed_QuelCabCombobox";
            this.ModMed_QuelCabCombobox.Size = new System.Drawing.Size(160, 21);
            this.ModMed_QuelCabCombobox.TabIndex = 45;
            // 
            // ModMed_BTN
            // 
            this.ModMed_BTN.Location = new System.Drawing.Point(290, 177);
            this.ModMed_BTN.Name = "ModMed_BTN";
            this.ModMed_BTN.Size = new System.Drawing.Size(100, 23);
            this.ModMed_BTN.TabIndex = 34;
            this.ModMed_BTN.Text = "Modifier";
            this.ModMed_BTN.UseVisualStyleBackColor = true;
            this.ModMed_BTN.Click += new System.EventHandler(this.ModMed_BTN_Click);
            // 
            // ModMedCab_Label
            // 
            this.ModMedCab_Label.AutoSize = true;
            this.ModMedCab_Label.Location = new System.Drawing.Point(225, 154);
            this.ModMedCab_Label.Name = "ModMedCab_Label";
            this.ModMedCab_Label.Size = new System.Drawing.Size(49, 13);
            this.ModMedCab_Label.TabIndex = 44;
            this.ModMedCab_Label.Text = "Cabinet :";
            this.ModMedCab_Label.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ModMedPrenom_Label
            // 
            this.ModMedPrenom_Label.AutoSize = true;
            this.ModMedPrenom_Label.Location = new System.Drawing.Point(225, 128);
            this.ModMedPrenom_Label.Name = "ModMedPrenom_Label";
            this.ModMedPrenom_Label.Size = new System.Drawing.Size(49, 13);
            this.ModMedPrenom_Label.TabIndex = 43;
            this.ModMedPrenom_Label.Text = "Prénom :";
            // 
            // ModMedNom_Label
            // 
            this.ModMedNom_Label.AutoSize = true;
            this.ModMedNom_Label.Location = new System.Drawing.Point(239, 102);
            this.ModMedNom_Label.Name = "ModMedNom_Label";
            this.ModMedNom_Label.Size = new System.Drawing.Size(35, 13);
            this.ModMedNom_Label.TabIndex = 42;
            this.ModMedNom_Label.Text = "Nom :";
            // 
            // ModMedPrenom_Textbox
            // 
            this.ModMedPrenom_Textbox.Location = new System.Drawing.Point(290, 125);
            this.ModMedPrenom_Textbox.Name = "ModMedPrenom_Textbox";
            this.ModMedPrenom_Textbox.Size = new System.Drawing.Size(100, 20);
            this.ModMedPrenom_Textbox.TabIndex = 27;
            // 
            // ModMedNom_Textbox
            // 
            this.ModMedNom_Textbox.Location = new System.Drawing.Point(290, 99);
            this.ModMedNom_Textbox.Name = "ModMedNom_Textbox";
            this.ModMedNom_Textbox.Size = new System.Drawing.Size(100, 20);
            this.ModMedNom_Textbox.TabIndex = 26;
            // 
            // ModMed_QuelMedTextbox
            // 
            this.ModMed_QuelMedTextbox.AutoSize = true;
            this.ModMed_QuelMedTextbox.Location = new System.Drawing.Point(83, 75);
            this.ModMed_QuelMedTextbox.Name = "ModMed_QuelMedTextbox";
            this.ModMed_QuelMedTextbox.Size = new System.Drawing.Size(191, 13);
            this.ModMed_QuelMedTextbox.TabIndex = 1;
            this.ModMed_QuelMedTextbox.Text = "Quel médecin souhaitez vous modifier :";
            // 
            // ModMed_QuelMedCombobox
            // 
            this.ModMed_QuelMedCombobox.FormattingEnabled = true;
            this.ModMed_QuelMedCombobox.Location = new System.Drawing.Point(290, 72);
            this.ModMed_QuelMedCombobox.Name = "ModMed_QuelMedCombobox";
            this.ModMed_QuelMedCombobox.Size = new System.Drawing.Size(160, 21);
            this.ModMed_QuelMedCombobox.TabIndex = 0;
            // 
            // ModCabCab_Label
            // 
            this.ModCabCab_Label.Controls.Add(this.label7);
            this.ModCabCab_Label.Controls.Add(this.ModCabQuelCab_Combobox);
            this.ModCabCab_Label.Controls.Add(this.ModCab_BTN);
            this.ModCabCab_Label.Controls.Add(this.ModCabGPS_Label);
            this.ModCabCab_Label.Controls.Add(this.ModCabAdresse_Label);
            this.ModCabCab_Label.Controls.Add(this.ModCabVille_Label);
            this.ModCabCab_Label.Controls.Add(this.ModCabGPS_Textbox);
            this.ModCabCab_Label.Controls.Add(this.ModCabVille_Textbox);
            this.ModCabCab_Label.Controls.Add(this.ModCabAdresse_Textbox);
            this.ModCabCab_Label.Location = new System.Drawing.Point(4, 22);
            this.ModCabCab_Label.Name = "ModCabCab_Label";
            this.ModCabCab_Label.Padding = new System.Windows.Forms.Padding(3);
            this.ModCabCab_Label.Size = new System.Drawing.Size(587, 309);
            this.ModCabCab_Label.TabIndex = 1;
            this.ModCabCab_Label.Text = "Modifier un cabinet";
            this.ModCabCab_Label.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(79, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(195, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Quel cabinet souhaitez vous modifier ? :";
            // 
            // ModCabQuelCab_Combobox
            // 
            this.ModCabQuelCab_Combobox.FormattingEnabled = true;
            this.ModCabQuelCab_Combobox.Location = new System.Drawing.Point(290, 73);
            this.ModCabQuelCab_Combobox.Name = "ModCabQuelCab_Combobox";
            this.ModCabQuelCab_Combobox.Size = new System.Drawing.Size(160, 21);
            this.ModCabQuelCab_Combobox.TabIndex = 6;
            this.ModCabQuelCab_Combobox.SelectedIndexChanged += new System.EventHandler(this.ModCabCab_Combobox_SelectedIndexChanged);
            // 
            // ModCab_BTN
            // 
            this.ModCab_BTN.Location = new System.Drawing.Point(290, 179);
            this.ModCab_BTN.Name = "ModCab_BTN";
            this.ModCab_BTN.Size = new System.Drawing.Size(100, 23);
            this.ModCab_BTN.TabIndex = 8;
            this.ModCab_BTN.Text = "Modifier";
            this.ModCab_BTN.Click += new System.EventHandler(this.ModCab_BTN_Click);
            // 
            // ModCabGPS_Label
            // 
            this.ModCabGPS_Label.AutoSize = true;
            this.ModCabGPS_Label.Location = new System.Drawing.Point(242, 156);
            this.ModCabGPS_Label.Name = "ModCabGPS_Label";
            this.ModCabGPS_Label.Size = new System.Drawing.Size(32, 13);
            this.ModCabGPS_Label.TabIndex = 5;
            this.ModCabGPS_Label.Text = "Gps :";
            // 
            // ModCabAdresse_Label
            // 
            this.ModCabAdresse_Label.AutoSize = true;
            this.ModCabAdresse_Label.Location = new System.Drawing.Point(223, 130);
            this.ModCabAdresse_Label.Name = "ModCabAdresse_Label";
            this.ModCabAdresse_Label.Size = new System.Drawing.Size(51, 13);
            this.ModCabAdresse_Label.TabIndex = 4;
            this.ModCabAdresse_Label.Text = "Adresse :";
            // 
            // ModCabVille_Label
            // 
            this.ModCabVille_Label.AutoSize = true;
            this.ModCabVille_Label.Location = new System.Drawing.Point(242, 104);
            this.ModCabVille_Label.Name = "ModCabVille_Label";
            this.ModCabVille_Label.Size = new System.Drawing.Size(32, 13);
            this.ModCabVille_Label.TabIndex = 3;
            this.ModCabVille_Label.Text = "Ville :";
            // 
            // ModCabGPS_Textbox
            // 
            this.ModCabGPS_Textbox.Location = new System.Drawing.Point(290, 153);
            this.ModCabGPS_Textbox.Name = "ModCabGPS_Textbox";
            this.ModCabGPS_Textbox.Size = new System.Drawing.Size(100, 20);
            this.ModCabGPS_Textbox.TabIndex = 2;
            // 
            // ModCabVille_Textbox
            // 
            this.ModCabVille_Textbox.Location = new System.Drawing.Point(290, 101);
            this.ModCabVille_Textbox.Name = "ModCabVille_Textbox";
            this.ModCabVille_Textbox.Size = new System.Drawing.Size(100, 20);
            this.ModCabVille_Textbox.TabIndex = 1;
            // 
            // ModCabAdresse_Textbox
            // 
            this.ModCabAdresse_Textbox.Location = new System.Drawing.Point(290, 127);
            this.ModCabAdresse_Textbox.Name = "ModCabAdresse_Textbox";
            this.ModCabAdresse_Textbox.Size = new System.Drawing.Size(100, 20);
            this.ModCabAdresse_Textbox.TabIndex = 0;
            // 
            // AjouterTab
            // 
            this.AjouterTab.Controls.Add(this.AddMenu);
            this.AjouterTab.Location = new System.Drawing.Point(4, 22);
            this.AjouterTab.Name = "AjouterTab";
            this.AjouterTab.Padding = new System.Windows.Forms.Padding(3);
            this.AjouterTab.Size = new System.Drawing.Size(607, 343);
            this.AjouterTab.TabIndex = 0;
            this.AjouterTab.Text = "Ajouter";
            this.AjouterTab.UseVisualStyleBackColor = true;
            // 
            // AddMenu
            // 
            this.AddMenu.Controls.Add(this.AddMedTab);
            this.AddMenu.Controls.Add(this.AddCabTab);
            this.AddMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddMenu.Location = new System.Drawing.Point(3, 3);
            this.AddMenu.Name = "AddMenu";
            this.AddMenu.SelectedIndex = 0;
            this.AddMenu.Size = new System.Drawing.Size(601, 337);
            this.AddMenu.TabIndex = 1;
            // 
            // AddMedTab
            // 
            this.AddMedTab.Controls.Add(this.AddMed_QuelCabCombobox);
            this.AddMedTab.Controls.Add(this.AddMedBTN);
            this.AddMedTab.Controls.Add(this.AddMedCab_Label);
            this.AddMedTab.Controls.Add(this.AddMedPrenom_Label);
            this.AddMedTab.Controls.Add(this.AddMedNom_Label);
            this.AddMedTab.Controls.Add(this.medecin_prenom);
            this.AddMedTab.Controls.Add(this.medecin_nom);
            this.AddMedTab.Location = new System.Drawing.Point(4, 22);
            this.AddMedTab.Name = "AddMedTab";
            this.AddMedTab.Padding = new System.Windows.Forms.Padding(3);
            this.AddMedTab.Size = new System.Drawing.Size(593, 311);
            this.AddMedTab.TabIndex = 0;
            this.AddMedTab.Text = "Ajouter médecin";
            this.AddMedTab.UseVisualStyleBackColor = true;
            // 
            // AddMed_QuelCabCombobox
            // 
            this.AddMed_QuelCabCombobox.FormattingEnabled = true;
            this.AddMed_QuelCabCombobox.Location = new System.Drawing.Point(301, 143);
            this.AddMed_QuelCabCombobox.Name = "AddMed_QuelCabCombobox";
            this.AddMed_QuelCabCombobox.Size = new System.Drawing.Size(160, 21);
            this.AddMed_QuelCabCombobox.TabIndex = 28;
            // 
            // AddMedBTN
            // 
            this.AddMedBTN.Location = new System.Drawing.Point(301, 169);
            this.AddMedBTN.Name = "AddMedBTN";
            this.AddMedBTN.Size = new System.Drawing.Size(100, 23);
            this.AddMedBTN.TabIndex = 25;
            this.AddMedBTN.Text = "Ajouter";
            this.AddMedBTN.UseVisualStyleBackColor = true;
            this.AddMedBTN.Click += new System.EventHandler(this.AddMedBTN_Click);
            // 
            // AddMedCab_Label
            // 
            this.AddMedCab_Label.AutoSize = true;
            this.AddMedCab_Label.Location = new System.Drawing.Point(137, 146);
            this.AddMedCab_Label.Name = "AddMedCab_Label";
            this.AddMedCab_Label.Size = new System.Drawing.Size(49, 13);
            this.AddMedCab_Label.TabIndex = 27;
            this.AddMedCab_Label.Text = "Cabinet :";
            // 
            // AddMedPrenom_Label
            // 
            this.AddMedPrenom_Label.AutoSize = true;
            this.AddMedPrenom_Label.Location = new System.Drawing.Point(137, 120);
            this.AddMedPrenom_Label.Name = "AddMedPrenom_Label";
            this.AddMedPrenom_Label.Size = new System.Drawing.Size(49, 13);
            this.AddMedPrenom_Label.TabIndex = 26;
            this.AddMedPrenom_Label.Text = "Prénom :";
            // 
            // AddMedNom_Label
            // 
            this.AddMedNom_Label.AutoSize = true;
            this.AddMedNom_Label.Location = new System.Drawing.Point(137, 94);
            this.AddMedNom_Label.Name = "AddMedNom_Label";
            this.AddMedNom_Label.Size = new System.Drawing.Size(35, 13);
            this.AddMedNom_Label.TabIndex = 25;
            this.AddMedNom_Label.Text = "Nom :";
            // 
            // medecin_prenom
            // 
            this.medecin_prenom.Location = new System.Drawing.Point(301, 117);
            this.medecin_prenom.Name = "medecin_prenom";
            this.medecin_prenom.Size = new System.Drawing.Size(100, 20);
            this.medecin_prenom.TabIndex = 18;
            // 
            // medecin_nom
            // 
            this.medecin_nom.Location = new System.Drawing.Point(301, 91);
            this.medecin_nom.Name = "medecin_nom";
            this.medecin_nom.Size = new System.Drawing.Size(100, 20);
            this.medecin_nom.TabIndex = 17;
            // 
            // AddCabTab
            // 
            this.AddCabTab.Controls.Add(this.AddCabVille_Textbox);
            this.AddCabTab.Controls.Add(this.AddCabBTN);
            this.AddCabTab.Controls.Add(this.AddCabVille_Label);
            this.AddCabTab.Location = new System.Drawing.Point(4, 22);
            this.AddCabTab.Name = "AddCabTab";
            this.AddCabTab.Padding = new System.Windows.Forms.Padding(3);
            this.AddCabTab.Size = new System.Drawing.Size(593, 311);
            this.AddCabTab.TabIndex = 1;
            this.AddCabTab.Text = "Ajouter un cabinet";
            this.AddCabTab.UseVisualStyleBackColor = true;
            // 
            // AddCabVille_Textbox
            // 
            this.AddCabVille_Textbox.Location = new System.Drawing.Point(302, 89);
            this.AddCabVille_Textbox.Name = "AddCabVille_Textbox";
            this.AddCabVille_Textbox.Size = new System.Drawing.Size(100, 20);
            this.AddCabVille_Textbox.TabIndex = 36;
            // 
            // AddCabBTN
            // 
            this.AddCabBTN.Location = new System.Drawing.Point(302, 115);
            this.AddCabBTN.Name = "AddCabBTN";
            this.AddCabBTN.Size = new System.Drawing.Size(100, 23);
            this.AddCabBTN.TabIndex = 38;
            this.AddCabBTN.Text = "Ajouter";
            this.AddCabBTN.UseVisualStyleBackColor = true;
            this.AddCabBTN.Click += new System.EventHandler(this.AddCabBTN_Click);
            // 
            // AddCabVille_Label
            // 
            this.AddCabVille_Label.AutoSize = true;
            this.AddCabVille_Label.Location = new System.Drawing.Point(138, 92);
            this.AddCabVille_Label.Name = "AddCabVille_Label";
            this.AddCabVille_Label.Size = new System.Drawing.Size(131, 13);
            this.AddCabVille_Label.TabIndex = 39;
            this.AddCabVille_Label.Text = "Code postal (ou Adresse) :";
            // 
            // AdminMenu
            // 
            this.AdminMenu.Controls.Add(this.AjouterTab);
            this.AdminMenu.Controls.Add(this.tabPage2);
            this.AdminMenu.Controls.Add(this.VtoMedTab);
            this.AdminMenu.Controls.Add(this.StatsQuelJour_Label);
            this.AdminMenu.Controls.Add(this.SupprVisiteTab);
            this.AdminMenu.Location = new System.Drawing.Point(12, 12);
            this.AdminMenu.Name = "AdminMenu";
            this.AdminMenu.SelectedIndex = 0;
            this.AdminMenu.Size = new System.Drawing.Size(615, 369);
            this.AdminMenu.TabIndex = 0;
            // 
            // VtoMedTab
            // 
            this.VtoMedTab.Controls.Add(this.Affect_BTN);
            this.VtoMedTab.Controls.Add(this.AffectMedecin_Label);
            this.VtoMedTab.Controls.Add(this.AffectVisiteur_Label);
            this.VtoMedTab.Controls.Add(this.AffectMedecin_ComboBox);
            this.VtoMedTab.Controls.Add(this.AffectVisiteur_ComboBox);
            this.VtoMedTab.Location = new System.Drawing.Point(4, 22);
            this.VtoMedTab.Name = "VtoMedTab";
            this.VtoMedTab.Size = new System.Drawing.Size(607, 343);
            this.VtoMedTab.TabIndex = 2;
            this.VtoMedTab.Text = "Affecter un visiteur à un médecin";
            this.VtoMedTab.UseVisualStyleBackColor = true;
            // 
            // Affect_BTN
            // 
            this.Affect_BTN.Location = new System.Drawing.Point(270, 170);
            this.Affect_BTN.Name = "Affect_BTN";
            this.Affect_BTN.Size = new System.Drawing.Size(75, 23);
            this.Affect_BTN.TabIndex = 4;
            this.Affect_BTN.Text = "Affecter";
            this.Affect_BTN.UseVisualStyleBackColor = true;
            this.Affect_BTN.Click += new System.EventHandler(this.Affect_BTN_Click);
            // 
            // AffectMedecin_Label
            // 
            this.AffectMedecin_Label.AutoSize = true;
            this.AffectMedecin_Label.Location = new System.Drawing.Point(354, 67);
            this.AffectMedecin_Label.Name = "AffectMedecin_Label";
            this.AffectMedecin_Label.Size = new System.Drawing.Size(82, 13);
            this.AffectMedecin_Label.TabIndex = 3;
            this.AffectMedecin_Label.Text = "Choisir Médecin";
            // 
            // AffectVisiteur_Label
            // 
            this.AffectVisiteur_Label.AutoSize = true;
            this.AffectVisiteur_Label.Location = new System.Drawing.Point(175, 67);
            this.AffectVisiteur_Label.Name = "AffectVisiteur_Label";
            this.AffectVisiteur_Label.Size = new System.Drawing.Size(75, 13);
            this.AffectVisiteur_Label.TabIndex = 2;
            this.AffectVisiteur_Label.Text = "Choisir Visiteur";
            // 
            // AffectMedecin_ComboBox
            // 
            this.AffectMedecin_ComboBox.FormattingEnabled = true;
            this.AffectMedecin_ComboBox.Location = new System.Drawing.Point(320, 90);
            this.AffectMedecin_ComboBox.Name = "AffectMedecin_ComboBox";
            this.AffectMedecin_ComboBox.Size = new System.Drawing.Size(160, 21);
            this.AffectMedecin_ComboBox.TabIndex = 1;
            // 
            // AffectVisiteur_ComboBox
            // 
            this.AffectVisiteur_ComboBox.FormattingEnabled = true;
            this.AffectVisiteur_ComboBox.Location = new System.Drawing.Point(130, 90);
            this.AffectVisiteur_ComboBox.Name = "AffectVisiteur_ComboBox";
            this.AffectVisiteur_ComboBox.Size = new System.Drawing.Size(160, 21);
            this.AffectVisiteur_ComboBox.TabIndex = 0;
            // 
            // StatsQuelJour_Label
            // 
            this.StatsQuelJour_Label.Controls.Add(this.StatsQuelJour_Labl);
            this.StatsQuelJour_Label.Controls.Add(this.StatsChoixJour_Picker);
            this.StatsQuelJour_Label.Controls.Add(this.panel2);
            this.StatsQuelJour_Label.Controls.Add(this.StatsQuelVisiteur_Label);
            this.StatsQuelJour_Label.Controls.Add(this.StatsQuelVisiteur_Combobox);
            this.StatsQuelJour_Label.Location = new System.Drawing.Point(4, 22);
            this.StatsQuelJour_Label.Name = "StatsQuelJour_Label";
            this.StatsQuelJour_Label.Size = new System.Drawing.Size(607, 343);
            this.StatsQuelJour_Label.TabIndex = 3;
            this.StatsQuelJour_Label.Text = "Statistiques";
            this.StatsQuelJour_Label.UseVisualStyleBackColor = true;
            // 
            // StatsQuelJour_Labl
            // 
            this.StatsQuelJour_Labl.AutoSize = true;
            this.StatsQuelJour_Labl.Location = new System.Drawing.Point(155, 97);
            this.StatsQuelJour_Labl.Name = "StatsQuelJour_Labl";
            this.StatsQuelJour_Labl.Size = new System.Drawing.Size(97, 13);
            this.StatsQuelJour_Labl.TabIndex = 21;
            this.StatsQuelJour_Labl.Text = "Choisissez un jour :";
            // 
            // StatsChoixJour_Picker
            // 
            this.StatsChoixJour_Picker.Location = new System.Drawing.Point(258, 91);
            this.StatsChoixJour_Picker.Name = "StatsChoixJour_Picker";
            this.StatsChoixJour_Picker.Size = new System.Drawing.Size(151, 20);
            this.StatsChoixJour_Picker.TabIndex = 20;
            this.StatsChoixJour_Picker.ValueChanged += new System.EventHandler(this.ChoixJour_Picker_ValueChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TempsAttenteJour_Content);
            this.panel2.Controls.Add(this.TempsChaqueVis_BTN);
            this.panel2.Controls.Add(this.TempsChaqueVis_Label);
            this.panel2.Controls.Add(this.VisiteJour_Content);
            this.panel2.Controls.Add(this.TempsAttenteJour_Label);
            this.panel2.Controls.Add(this.VisiteJour_Label);
            this.panel2.Controls.Add(this.TempsAttTotal_Content);
            this.panel2.Controls.Add(this.TempsAttTotal_Label);
            this.panel2.Location = new System.Drawing.Point(35, 137);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(544, 175);
            this.panel2.TabIndex = 18;
            // 
            // TempsAttenteJour_Content
            // 
            this.TempsAttenteJour_Content.AutoSize = true;
            this.TempsAttenteJour_Content.Location = new System.Drawing.Point(178, 125);
            this.TempsAttenteJour_Content.Name = "TempsAttenteJour_Content";
            this.TempsAttenteJour_Content.Size = new System.Drawing.Size(10, 13);
            this.TempsAttenteJour_Content.TabIndex = 22;
            this.TempsAttenteJour_Content.Text = "-";
            // 
            // TempsChaqueVis_BTN
            // 
            this.TempsChaqueVis_BTN.Location = new System.Drawing.Point(456, 75);
            this.TempsChaqueVis_BTN.Name = "TempsChaqueVis_BTN";
            this.TempsChaqueVis_BTN.Size = new System.Drawing.Size(52, 23);
            this.TempsChaqueVis_BTN.TabIndex = 11;
            this.TempsChaqueVis_BTN.Text = "Voir";
            this.TempsChaqueVis_BTN.UseVisualStyleBackColor = true;
            this.TempsChaqueVis_BTN.Click += new System.EventHandler(this.TempsAttenteTotal_BTN_Click);
            // 
            // TempsChaqueVis_Label
            // 
            this.TempsChaqueVis_Label.AutoSize = true;
            this.TempsChaqueVis_Label.Location = new System.Drawing.Point(359, 80);
            this.TempsChaqueVis_Label.Name = "TempsChaqueVis_Label";
            this.TempsChaqueVis_Label.Size = new System.Drawing.Size(91, 13);
            this.TempsChaqueVis_Label.TabIndex = 16;
            this.TempsChaqueVis_Label.Text = "Toutes ses visites";
            // 
            // VisiteJour_Content
            // 
            this.VisiteJour_Content.AutoSize = true;
            this.VisiteJour_Content.Location = new System.Drawing.Point(161, 80);
            this.VisiteJour_Content.Name = "VisiteJour_Content";
            this.VisiteJour_Content.Size = new System.Drawing.Size(10, 13);
            this.VisiteJour_Content.TabIndex = 21;
            this.VisiteJour_Content.Text = "-";
            // 
            // TempsAttenteJour_Label
            // 
            this.TempsAttenteJour_Label.AutoSize = true;
            this.TempsAttenteJour_Label.Location = new System.Drawing.Point(24, 125);
            this.TempsAttenteJour_Label.Name = "TempsAttenteJour_Label";
            this.TempsAttenteJour_Label.Size = new System.Drawing.Size(148, 13);
            this.TempsAttenteJour_Label.TabIndex = 21;
            this.TempsAttenteJour_Label.Text = "Temps d\'attente pour ce jour :";
            // 
            // VisiteJour_Label
            // 
            this.VisiteJour_Label.AutoSize = true;
            this.VisiteJour_Label.Location = new System.Drawing.Point(24, 80);
            this.VisiteJour_Label.Name = "VisiteJour_Label";
            this.VisiteJour_Label.Size = new System.Drawing.Size(131, 13);
            this.VisiteJour_Label.TabIndex = 20;
            this.VisiteJour_Label.Text = "Visites effectuées ce jour :";
            // 
            // TempsAttTotal_Content
            // 
            this.TempsAttTotal_Content.AutoSize = true;
            this.TempsAttTotal_Content.Location = new System.Drawing.Point(233, 47);
            this.TempsAttTotal_Content.Name = "TempsAttTotal_Content";
            this.TempsAttTotal_Content.Size = new System.Drawing.Size(10, 13);
            this.TempsAttTotal_Content.TabIndex = 19;
            this.TempsAttTotal_Content.Text = "-";
            // 
            // TempsAttTotal_Label
            // 
            this.TempsAttTotal_Label.AutoSize = true;
            this.TempsAttTotal_Label.Location = new System.Drawing.Point(220, 21);
            this.TempsAttTotal_Label.Name = "TempsAttTotal_Label";
            this.TempsAttTotal_Label.Size = new System.Drawing.Size(119, 13);
            this.TempsAttTotal_Label.TabIndex = 0;
            this.TempsAttTotal_Label.Text = "Temps d\'attente Total : ";
            // 
            // StatsQuelVisiteur_Label
            // 
            this.StatsQuelVisiteur_Label.AutoSize = true;
            this.StatsQuelVisiteur_Label.Location = new System.Drawing.Point(145, 58);
            this.StatsQuelVisiteur_Label.Name = "StatsQuelVisiteur_Label";
            this.StatsQuelVisiteur_Label.Size = new System.Drawing.Size(107, 13);
            this.StatsQuelVisiteur_Label.TabIndex = 13;
            this.StatsQuelVisiteur_Label.Text = "Choisissez un visiteur";
            // 
            // StatsQuelVisiteur_Combobox
            // 
            this.StatsQuelVisiteur_Combobox.FormattingEnabled = true;
            this.StatsQuelVisiteur_Combobox.Location = new System.Drawing.Point(258, 55);
            this.StatsQuelVisiteur_Combobox.Name = "StatsQuelVisiteur_Combobox";
            this.StatsQuelVisiteur_Combobox.Size = new System.Drawing.Size(151, 21);
            this.StatsQuelVisiteur_Combobox.TabIndex = 12;
            this.StatsQuelVisiteur_Combobox.SelectedIndexChanged += new System.EventHandler(this.ChoixVisiteur_Combobox_SelectedIndexChanged);
            // 
            // disconnectBTN
            // 
            this.disconnectBTN.Location = new System.Drawing.Point(542, 5);
            this.disconnectBTN.Name = "disconnectBTN";
            this.disconnectBTN.Size = new System.Drawing.Size(81, 23);
            this.disconnectBTN.TabIndex = 29;
            this.disconnectBTN.Text = "Déconnexion";
            this.disconnectBTN.UseVisualStyleBackColor = true;
            this.disconnectBTN.Click += new System.EventHandler(this.disconnectBTN_Click);
            // 
            // SupprVisiteTab
            // 
            this.SupprVisiteTab.Controls.Add(this.Visites);
            this.SupprVisiteTab.Location = new System.Drawing.Point(4, 22);
            this.SupprVisiteTab.Name = "SupprVisiteTab";
            this.SupprVisiteTab.Padding = new System.Windows.Forms.Padding(3);
            this.SupprVisiteTab.Size = new System.Drawing.Size(607, 343);
            this.SupprVisiteTab.TabIndex = 4;
            this.SupprVisiteTab.Text = "Supprimer Visites";
            this.SupprVisiteTab.UseVisualStyleBackColor = true;
            // 
            // Visites
            // 
            this.Visites.Controls.Add(this.AllVisites_Tab);
            this.Visites.Controls.Add(this.tabPage4);
            this.Visites.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Visites.Location = new System.Drawing.Point(3, 3);
            this.Visites.Name = "Visites";
            this.Visites.SelectedIndex = 0;
            this.Visites.Size = new System.Drawing.Size(601, 337);
            this.Visites.TabIndex = 0;
            // 
            // AllVisites_Tab
            // 
            this.AllVisites_Tab.Controls.Add(this.DatagridviewVisite);
            this.AllVisites_Tab.Location = new System.Drawing.Point(4, 22);
            this.AllVisites_Tab.Name = "AllVisites_Tab";
            this.AllVisites_Tab.Padding = new System.Windows.Forms.Padding(3);
            this.AllVisites_Tab.Size = new System.Drawing.Size(593, 311);
            this.AllVisites_Tab.TabIndex = 0;
            this.AllVisites_Tab.Text = "Toutes";
            this.AllVisites_Tab.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.VisiteQuelVisiteur_Label);
            this.tabPage4.Controls.Add(this.VisiteQuelVisiteur_Combobox);
            this.tabPage4.Controls.Add(this.dataGridViewVisiteChoix);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(593, 311);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Choix Visiteur";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // DatagridviewVisite
            // 
            this.DatagridviewVisite.AllowUserToAddRows = false;
            this.DatagridviewVisite.AllowUserToOrderColumns = true;
            this.DatagridviewVisite.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.DatagridviewVisite.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.DatagridviewVisite.BackgroundColor = System.Drawing.Color.White;
            this.DatagridviewVisite.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DatagridviewVisite.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Supprimer});
            this.DatagridviewVisite.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DatagridviewVisite.Location = new System.Drawing.Point(3, 3);
            this.DatagridviewVisite.Name = "DatagridviewVisite";
            this.DatagridviewVisite.RowHeadersVisible = false;
            this.DatagridviewVisite.Size = new System.Drawing.Size(587, 305);
            this.DatagridviewVisite.TabIndex = 3;
            // 
            // Supprimer
            // 
            this.Supprimer.HeaderText = "Supprimer";
            this.Supprimer.Name = "Supprimer";
            this.Supprimer.Text = "Supprimer";
            this.Supprimer.ToolTipText = "Supprimer";
            this.Supprimer.UseColumnTextForButtonValue = true;
            this.Supprimer.Width = 60;
            // 
            // dataGridViewVisiteChoix
            // 
            this.dataGridViewVisiteChoix.AllowUserToAddRows = false;
            this.dataGridViewVisiteChoix.AllowUserToOrderColumns = true;
            this.dataGridViewVisiteChoix.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewVisiteChoix.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewVisiteChoix.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewVisiteChoix.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridViewVisiteChoix.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewButtonColumn1});
            this.dataGridViewVisiteChoix.Location = new System.Drawing.Point(6, 55);
            this.dataGridViewVisiteChoix.Name = "dataGridViewVisiteChoix";
            this.dataGridViewVisiteChoix.RowHeadersVisible = false;
            this.dataGridViewVisiteChoix.Size = new System.Drawing.Size(584, 253);
            this.dataGridViewVisiteChoix.TabIndex = 3;
            // 
            // dataGridViewButtonColumn1
            // 
            this.dataGridViewButtonColumn1.HeaderText = "Supprimer";
            this.dataGridViewButtonColumn1.Name = "dataGridViewButtonColumn1";
            this.dataGridViewButtonColumn1.Text = "Supprimer";
            this.dataGridViewButtonColumn1.ToolTipText = "Supprimer";
            this.dataGridViewButtonColumn1.UseColumnTextForButtonValue = true;
            this.dataGridViewButtonColumn1.Width = 60;
            // 
            // VisiteQuelVisiteur_Label
            // 
            this.VisiteQuelVisiteur_Label.AutoSize = true;
            this.VisiteQuelVisiteur_Label.Location = new System.Drawing.Point(158, 21);
            this.VisiteQuelVisiteur_Label.Name = "VisiteQuelVisiteur_Label";
            this.VisiteQuelVisiteur_Label.Size = new System.Drawing.Size(107, 13);
            this.VisiteQuelVisiteur_Label.TabIndex = 15;
            this.VisiteQuelVisiteur_Label.Text = "Choisissez un visiteur";
            // 
            // VisiteQuelVisiteur_Combobox
            // 
            this.VisiteQuelVisiteur_Combobox.FormattingEnabled = true;
            this.VisiteQuelVisiteur_Combobox.Location = new System.Drawing.Point(271, 18);
            this.VisiteQuelVisiteur_Combobox.Name = "VisiteQuelVisiteur_Combobox";
            this.VisiteQuelVisiteur_Combobox.Size = new System.Drawing.Size(151, 21);
            this.VisiteQuelVisiteur_Combobox.TabIndex = 14;
            // 
            // FormAdministration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(639, 393);
            this.Controls.Add(this.disconnectBTN);
            this.Controls.Add(this.AdminMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormAdministration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Espace administration";
            this.Load += new System.EventHandler(this.FormAdministration_Load);
            this.tabPage2.ResumeLayout(false);
            this.ModifyTab.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ModCabCab_Label.ResumeLayout(false);
            this.ModCabCab_Label.PerformLayout();
            this.AjouterTab.ResumeLayout(false);
            this.AddMenu.ResumeLayout(false);
            this.AddMedTab.ResumeLayout(false);
            this.AddMedTab.PerformLayout();
            this.AddCabTab.ResumeLayout(false);
            this.AddCabTab.PerformLayout();
            this.AdminMenu.ResumeLayout(false);
            this.VtoMedTab.ResumeLayout(false);
            this.VtoMedTab.PerformLayout();
            this.StatsQuelJour_Label.ResumeLayout(false);
            this.StatsQuelJour_Label.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.SupprVisiteTab.ResumeLayout(false);
            this.Visites.ResumeLayout(false);
            this.AllVisites_Tab.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DatagridviewVisite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewVisiteChoix)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabControl ModifyTab;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ComboBox ModMed_QuelCabCombobox;
        private System.Windows.Forms.Button ModMed_BTN;
        private System.Windows.Forms.Label ModMedCab_Label;
        private System.Windows.Forms.Label ModMedPrenom_Label;
        private System.Windows.Forms.Label ModMedNom_Label;
        private System.Windows.Forms.TextBox ModMedPrenom_Textbox;
        private System.Windows.Forms.TextBox ModMedNom_Textbox;
        private System.Windows.Forms.Label ModMed_QuelMedTextbox;
        private System.Windows.Forms.ComboBox ModMed_QuelMedCombobox;
        private System.Windows.Forms.TabPage ModCabCab_Label;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox ModCabQuelCab_Combobox;
        private System.Windows.Forms.Button ModCab_BTN;
        private System.Windows.Forms.Label ModCabGPS_Label;
        private System.Windows.Forms.Label ModCabAdresse_Label;
        private System.Windows.Forms.Label ModCabVille_Label;
        private System.Windows.Forms.TextBox ModCabGPS_Textbox;
        private System.Windows.Forms.TextBox ModCabVille_Textbox;
        private System.Windows.Forms.TextBox ModCabAdresse_Textbox;
        private System.Windows.Forms.TabPage AjouterTab;
        private System.Windows.Forms.TabControl AddMenu;
        private System.Windows.Forms.TabPage AddMedTab;
        private System.Windows.Forms.ComboBox AddMed_QuelCabCombobox;
        private System.Windows.Forms.Button AddMedBTN;
        private System.Windows.Forms.Label AddMedCab_Label;
        private System.Windows.Forms.Label AddMedPrenom_Label;
        private System.Windows.Forms.Label AddMedNom_Label;
        private System.Windows.Forms.TextBox medecin_prenom;
        private System.Windows.Forms.TextBox medecin_nom;
        private System.Windows.Forms.TabPage AddCabTab;
        private System.Windows.Forms.TabControl AdminMenu;
        private System.Windows.Forms.TabPage VtoMedTab;
        private System.Windows.Forms.TabPage StatsQuelJour_Label;
        private System.Windows.Forms.TextBox AddCabVille_Textbox;
        private System.Windows.Forms.Button AddCabBTN;
        private System.Windows.Forms.Label AddCabVille_Label;
        private System.Windows.Forms.Button disconnectBTN;
        private System.Windows.Forms.ComboBox StatsQuelVisiteur_Combobox;
        private System.Windows.Forms.Button TempsChaqueVis_BTN;
        private System.Windows.Forms.Label StatsQuelVisiteur_Label;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label TempsAttTotal_Label;
        private System.Windows.Forms.Label TempsChaqueVis_Label;
        private System.Windows.Forms.Label TempsAttTotal_Content;
        private System.Windows.Forms.Label TempsAttenteJour_Label;
        private System.Windows.Forms.DateTimePicker StatsChoixJour_Picker;
        private System.Windows.Forms.Label TempsAttenteJour_Content;
        private System.Windows.Forms.Label AffectMedecin_Label;
        private System.Windows.Forms.Label AffectVisiteur_Label;
        private System.Windows.Forms.ComboBox AffectMedecin_ComboBox;
        private System.Windows.Forms.ComboBox AffectVisiteur_ComboBox;
        private System.Windows.Forms.Button Affect_BTN;
        private System.Windows.Forms.Label StatsQuelJour_Labl;
        private System.Windows.Forms.Label VisiteJour_Content;
        private System.Windows.Forms.Label VisiteJour_Label;
        private System.Windows.Forms.TabPage SupprVisiteTab;
        private System.Windows.Forms.TabControl Visites;
        private System.Windows.Forms.TabPage AllVisites_Tab;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView DatagridviewVisite;
        private System.Windows.Forms.DataGridViewButtonColumn Supprimer;
        private System.Windows.Forms.DataGridView dataGridViewVisiteChoix;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn1;
        private System.Windows.Forms.Label VisiteQuelVisiteur_Label;
        private System.Windows.Forms.ComboBox VisiteQuelVisiteur_Combobox;
    }
}