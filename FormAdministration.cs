﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace SuiviA
{
    public partial class FormAdministration : Form
    {
        private List<Medecin> medecins;
        private List<Cabinet> cabinets;
        private List<Visiteur> visiteurs;

        public string SelectedUser = "";
        public string SelectedDay = "";

        public FormAdministration()
        {
            InitializeComponent();
        }


        private void FormAdministration_Load(object sender, EventArgs e)
        {
            StatsChoixJour_Picker.Format = DateTimePickerFormat.Custom; //Les lignes suivantes applique un format au composant DataTimePicker
            StatsChoixJour_Picker.CustomFormat = "yyyy-MM-dd"; //Format Date en année-mois-jour

            LoadComboBox(); //Charge les combobox et initialise les lites cab,med
            DisplayStats(); //Charge les calculs statistiques

            ModMed_QuelMedCombobox.SelectedIndexChanged += comboBoxModMed_SelectedIndexChanged;
            ModCabQuelCab_Combobox.SelectedIndexChanged += ModCabCab_Combobox_SelectedIndexChanged;

            BindGrid();
        }

        private void BindGrid() // Affiche les visites dans le datagridview
        {
            Cursor.Current = Cursors.WaitCursor;

            DatagridviewVisite.DataSource = Service.getVisiteAll();

          //  dataGridViewVisiteChoix.DataSource = Service.getVisiteID(SelectedUser);

            Cursor.Current = Cursors.Default;
            // DatagridviewVisite.Columns[2].DefaultCellStyle.Format = "dd/MM/yyyy";
        }


        private void ModCabCab_Combobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            CompleteTextCabinet();
        }

        private void comboBoxModMed_SelectedIndexChanged(object sender, EventArgs e)
        {
            CompleteTextMedecin();
        }

        private void CompleteTextMedecin()
        {
            medecins.ForEach(m =>
            {

                if (m.idMedecin.ToString().TrimEnd() == ModMed_QuelMedCombobox.SelectedItem.ToString().Split('-')[0].Trim())
                {
                    ModMedNom_Textbox.Text = m.nom;
                    ModMedPrenom_Textbox.Text = m.prenom;
                }
            });
        }

        private void CompleteTextCabinet()
        {
            cabinets.ForEach(c =>
            {
                if (c.idCabinet.ToString().Trim() == ModCabQuelCab_Combobox.SelectedItem.ToString().Split('-')[0].Trim())
                {
                    ModCabVille_Textbox.Text = c.ville;
                    ModCabAdresse_Textbox.Text = c.adresse;
                    ModCabGPS_Textbox.Text = c.gps;
                }
            });
        }

        private void LoadComboBox()
        {

            ModMed_QuelMedCombobox.Items.Clear();//Modifier Medecin , Choix du Médecins

            ModMed_QuelCabCombobox.Items.Clear();//Modifier Medecin , Choix du Cabinet  

            ModCabQuelCab_Combobox.Items.Clear();//Modifier Cabinet , Choix du Cabinet  

            AddMed_QuelCabCombobox.Items.Clear();//Ajouter Medecin , Choix du Cabinet

            StatsQuelVisiteur_Combobox.Items.Clear();//Statistiques, Choix de l'utilisateur

            AffectMedecin_ComboBox.Items.Clear();//Affecter visiteur à medecin, choix du medecin

            AffectVisiteur_ComboBox.Items.Clear();//Affecter visiteur à medecin, choix du visiteur


            medecins = Service.getMedecin();
            medecins.ForEach(m =>
            {
                AffectMedecin_ComboBox.Items.Add(m.idMedecin + " - " + m.nom + " " + m.prenom);
                ModMed_QuelMedCombobox.Items.Add(m.idMedecin + " - " + m.nom + " " + m.prenom);

            });
            ModMed_QuelMedCombobox.SelectedIndex = 0;
            AffectMedecin_ComboBox.SelectedIndex = 0;


            cabinets = Service.getCabinet();

            cabinets.ForEach(c =>
            {


                ModMed_QuelCabCombobox.Items.Add(c.idCabinet + " - " + c.ville);


                ModCabQuelCab_Combobox.Items.Add(c.idCabinet + " - " + c.ville);


                AddMed_QuelCabCombobox.Items.Add(c.idCabinet + " - " + c.ville);

            });
            ModMed_QuelCabCombobox.SelectedIndex = 0;
            ModCabQuelCab_Combobox.SelectedIndex = 0;
            AddMed_QuelCabCombobox.SelectedIndex = 0;



            visiteurs = Service.getVisiteurs();
            visiteurs.ForEach(v =>
            {
                AffectVisiteur_ComboBox.Items.Add(v.id + " - " + v.nom + " " + v.prenom);
                StatsQuelVisiteur_Combobox.Items.Add(v.id + " - " + v.nom + " " + v.prenom);

            });
            StatsQuelVisiteur_Combobox.SelectedIndex = 1;//visiteur par défaut
            AffectVisiteur_ComboBox.SelectedIndex = 0;

            CompleteTextMedecin();
            CompleteTextCabinet();

        }


        private void AddMedBTN_Click(object sender, EventArgs e)
        {
            if (medecin_prenom.Text != "" && medecin_nom.Text != "")
            {
                try
                {
                    Medecin medecin = new Medecin(medecin_nom.Text, medecin_prenom.Text, Int32.Parse(AddMed_QuelCabCombobox.SelectedItem.ToString().Split('-')[0].Trim()));
                    Service.post("api/medecin", medecin);
                    MessageBox.Show("Medecin ajoutée !");
                    LoadComboBox();
                }
                catch { MessageBox.Show("Une erreur est survenue \n  -  Vérifiez la connexion au serveur local ou à la base de données", "Attention!"); }
            }
        }

        private void AddCabBTN_Click(object sender, EventArgs e)
        {
            //  MessageBox.Show(Service.getGps("59000").Results[0].AddressComponents[1].LongName); 

            if (AddCabVille_Textbox.Text != "")
            {
                try
                {
                    Gps gps = Service.getGps(AddCabVille_Textbox.Text);
                    Result final_gps = gps.Results[0];

                    string displayAdress = "\t\t\t" + final_gps.FormattedAddress + "\t\t\t";
                    string MsgboxContent = "Voici l'adresse proposée par l'API google maps: \n\n" + displayAdress + "\n \n Si ce n'est pas assez précis : \n\n   - Tapez l'adresse sur google map sous forme '1 Rue Nationale, Lille' \n";
                    string MsgboxContentDown = "\n --------------------------------------------------------------- \n Ville : " + final_gps.AddressComponents[1].LongName + "\n --------------------------------------------------------------- \n Position : " + final_gps.Geometry.Location.Lng.ToString();
                    string instruction = "\n\n\n\n  Oui pour ajouter le cabinet, Non pour recommencer";

                    DialogResult dialogResult = MessageBox.Show(MsgboxContent + MsgboxContentDown + instruction, "Conseil", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        Cabinet cabinet = new Cabinet(final_gps.AddressComponents[1].LongName, final_gps.FormattedAddress, final_gps.Geometry.Location.Lat.ToString() + ";" + final_gps.Geometry.Location.Lng.ToString());
                        Service.post("api/cabinet", cabinet);
                        MessageBox.Show("Cabinet ajoutée !");
                        LoadComboBox();
                    }               
                } 
                catch { MessageBox.Show("Une erreur est survenue \n  -  Vérifiez la connexion au serveur local ou à la base de données", "Attention!"); }
            }
        }


        private void disconnectBTN_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormConnexion f1 = new FormConnexion();
            f1.ShowDialog();
        }



        private void ModCab_BTN_Click(object sender, EventArgs e)
        {
            if (ModCabVille_Textbox.Text != "" && ModCabAdresse_Textbox.Text != "" && ModCabGPS_Textbox.Text != "")
            {
                try
                {
                    Cabinet cabinet = new Cabinet(Int32.Parse(ModCabQuelCab_Combobox.SelectedItem.ToString().Split('-')[0].Trim()), ModCabVille_Textbox.Text, ModCabAdresse_Textbox.Text, ModCabGPS_Textbox.Text);
                    Service.put("api/cabinet", cabinet);
                    MessageBox.Show("Cabinet modifié !");
                    LoadComboBox();
                }
                catch { MessageBox.Show("Une erreur est survenue \n  -  Vérifiez la connexion au serveur local ou à la base de données", "Attention!"); }
                CompleteTextCabinet();
            }
        }



        private void ModMed_BTN_Click(object sender, EventArgs e)
        {
            if (ModMedPrenom_Textbox.Text != "" && ModMedNom_Textbox.Text != "")
            {
                try
                {
                    Medecin medecin = new Medecin(Int32.Parse(ModMed_QuelMedCombobox.SelectedItem.ToString().Split('-')[0].Trim()), ModMedNom_Textbox.Text, ModMedPrenom_Textbox.Text, Int32.Parse(ModMed_QuelCabCombobox.SelectedItem.ToString().Split('-')[0].Trim()));
                    Service.put("api/medecin", medecin);
                    MessageBox.Show("Medecin modifié !");
                    LoadComboBox();
                }
                catch { MessageBox.Show("Une erreur est survenue \n  -  Vérifiez la connexion au serveur local ou à la base de données", "Attention!"); }
            }
        }


        private void ChoixVisiteur_Combobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisplayStats();
        }

        private void ChoixJour_Picker_ValueChanged(object sender, EventArgs e)//Temps d'attente total pour ce jour et cet user
        {
            DisplayStats();
        }





        private void DisplayStats()//temps total
        {
            SelectedUser = StatsQuelVisiteur_Combobox.SelectedItem.ToString().Split('-')[0].Trim();
            SelectedDay = StatsChoixJour_Picker.Text;
            try
            {            
                List<Timetotal> timetots;

                timetots = Service.GetTotalWait(SelectedUser);
                timetots.ForEach(t =>   
                {
                    TempsAttTotal_Content.Text = t.sumtime.ToString();
                }); //Label temps total 
            }
            catch {
               //MessageBox.Show("Une erreur est survenue \n  -  Vérifiez la connexion au serveur local ou à la base de données", "Attention!");
            }

            List<TimetotalJour> TTJ;
            List<TimetotalJour> VisiteJour;

            try
            {
                TTJ = Service.GetTimeTotalJour(SelectedUser, SelectedDay);
                TTJ.ForEach(t =>
                {
                    TempsAttenteJour_Content.Text = "";
                    TempsAttenteJour_Content.Text = t.TempsTotalJour;
                });

                VisiteJour = Service.getNbrVisiteparJour(SelectedUser, StatsChoixJour_Picker.Text);
                VisiteJour.ForEach(t =>
                {
                 //   MessageBox.Show(t.TempsTotalJour);
                    VisiteJour_Content.Text = t.TempsTotalJour;
                });
            }
            catch {
                //MessageBox.Show("Une erreur est survenue \n  -  Vérifiez la connexion au serveur local ou à la base de données", "Attention!"); 
                 }
        }


 


        private void TempsAttenteTotal_BTN_Click(object sender, EventArgs e)//Datagrid view Temps d'attente pour chaque visite
        {
            try
            {
                FormStatistiques f_TAT = new FormStatistiques();

                f_TAT.stat.DataSource = Service.GetTimeDiff(SelectedUser);
                SuiviA.Statistiques.WhatStat = "Temps d'attente total";
                f_TAT.Show();
            }
            catch
            {
                MessageBox.Show("Une erreur est survenue \n  -  Vérifiez la connexion au serveur local ou à la base de données", "Attention!");
            }
        }



        private void Affect_BTN_Click(object sender, EventArgs e)
        {
            string idVis = AffectVisiteur_ComboBox.SelectedItem.ToString().Split('-')[0].Trim();
            int idMed = Convert.ToInt32(AffectMedecin_ComboBox.SelectedItem.ToString().Split('-')[0].Trim());

            try
            {
                medecins = Service.getMedecin();
                medecins.ForEach(m =>
                {
                    if (m.idMedecin == idMed)
                    {
                        Medecin med = new Medecin(m.idMedecin, m.nom, m.prenom, m.idcabinet);
                        Service.put("api/affectvisiteur/" + idVis, med);
                    }
                });
            }
            catch { MessageBox.Show("Une erreur est survenue \n  -  Vérifiez la connexion au serveur local ou à la base de données", "Attention!"); }      
        }

        private void RdvCheckbox_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void AjoutVisiteBtn_Click(object sender, EventArgs e)
        {

        }

        private void StatsChoixJour_Picker_ValueChanged(object sender, EventArgs e)
        {

        }

        private void TempsChaqueVis_BTN_Click(object sender, EventArgs e)
        {

        }
    }
}
