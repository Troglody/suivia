﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Net.WebRequestMethods;
using EasyHttp.Http;


namespace SuiviA
{
    public partial class FormStatistiques : Form
    {
        public DataGridView stat { get; set; }


        public FormStatistiques()
        {
            stat = new DataGridView();
            this.CenterToScreen();
            InitializeComponent();
    
        }

        private void FormStatistiques_Load(object sender, EventArgs e)
        {
            StatDataGridView.DataSource = stat.DataSource;
            StatDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill; 
 
            WhatStat_Groupbox.Text = Statistiques.WhatStat;
            //StatDataGridView.DataSource = Statistiques.DataSource;
    
        }

 
    }
}
